package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.listener.SceneStatusListener
import com.dimdarkevil.noitaworld.model.*
import com.dimdarkevil.noitaworld.cellrenderer.BiomeTableColorCellRenderer
import com.dimdarkevil.noitaworld.cellrenderer.EntityTableImgCellRenderer
import com.dimdarkevil.noitaworld.io.NoitaData
import com.dimdarkevil.noitaworld.tablemodel.*
import com.dimdarkevil.swingutil.*
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ComponentEvent
import java.awt.event.KeyEvent
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.border.BevelBorder
import javax.swing.border.EmptyBorder
import javax.swing.event.ListSelectionEvent

class SceneForm(
	private val frame: JFrame,
	private val config: AppConfig,
	private val project: Project,
	private val scene: Scene,
	private val noitaData: NoitaData,
) : ComponentListenerAdapter, SceneStatusListener {
	val mainPanel = JPanel()
	private val sceneDrawPanel = SceneDrawPanel(this)
	private val tabPane = JTabbedPane()

	private val matPanel = JPanel(BorderLayout())
	private val matSearchTextField = JTextField()
	private val matTableModel = MaterialTableModel()
	private val matTable = JTable(matTableModel)
	private val matScrollPane = JScrollPane(matTable)

	private val entPanel = JPanel(BorderLayout())
	private val entAddBtn = JButton("add entity")
	private val entSearchTextField = JTextField()
	private val entTableModel = EntityTableModel()
	private val entTable = JTable(entTableModel)
	private val entScrollPane = JScrollPane(entTable)

	private val spellPanel = JPanel(BorderLayout())
	private val spellAddBtn = JButton("add spell")
	private val spellSearchTextField = JTextField()
	private val spellTableModel = SpellTableModel()
	private val spellTable = JTable(spellTableModel)
	private val spellScrollPane = JScrollPane(spellTable)

	private val perkPanel = JPanel(BorderLayout())
	private val perkAddBtn = JButton("add perk")
	private val perkSearchTextField = JTextField()
	private val perkTableModel = PerkTableModel()
	private val perkTable = JTable(perkTableModel)
	private val perkScrollPane = JScrollPane(perkTable)

	private val specialPanel = JPanel(BorderLayout())
	private val specialAddBtn = JButton("add special")
	private val specialSearchTextField = JTextField()
	private val specialTableModel = SpecialTableModel()
	private val specialTable = JTable(specialTableModel)
	private val specialScrollPane = JScrollPane(specialTable)

	private val objPanel = JPanel()
	private val objDelBtn = JButton("delete object")
	private val objTableModel = ObjectTableModel()
	private val objTable = JTable(objTableModel)
	private val objScrollPane = JScrollPane(objTable)

	private val sidePanel = JSplitPane(JSplitPane.VERTICAL_SPLIT, tabPane, objPanel)
	private val matColorPanel = JPanel()
	private val matTextLabel = JLabel("")
	private val eyeColorPanel = JPanel()
	private val eyeTextLabel = JLabel("")
	private val replaceBtn = JButton("replace")

	private val splitPane = JSplitPane(JSplitPane.HORIZONTAL_SPLIT, sceneDrawPanel, sidePanel)
	private val imageXLabel = JLabel("0")
	private val imageYLabel = JLabel("0")
	private val imageColorLabel = JLabel(" ")

	private var selectedMat: Material? = null
	private var replacementColor: Int? = null
	//private val statLabel = JLabel("***")


	init {
		mainPanel.layout = BorderLayout(0, 0)

		val matSearchPanel = PaddedPanel()
		matSearchPanel.layout = BoxLayout(matSearchPanel, BoxLayout.X_AXIS)
		matSearchPanel.add(JLabel("search:"))
		matSearchPanel.add(matSearchTextField)
		matPanel.add(matSearchPanel, BorderLayout.NORTH)
		matPanel.add(matScrollPane, BorderLayout.CENTER)

		val entSearchPanel = PaddedPanel()
		entSearchPanel.layout = BoxLayout(entSearchPanel, BoxLayout.X_AXIS)
		entSearchPanel.add(JLabel("search:"))
		entSearchPanel.add(entSearchTextField)
		val entBtnPanel = PaddedPanel()
		entBtnPanel.layout = BoxLayout(entBtnPanel, BoxLayout.X_AXIS)
		entBtnPanel.add(entAddBtn)
		entPanel.add(entSearchPanel, BorderLayout.NORTH)
		entPanel.add(entScrollPane, BorderLayout.CENTER)
		entPanel.add(entBtnPanel, BorderLayout.SOUTH)

		val spellSearchPanel = PaddedPanel()
		spellSearchPanel.layout = BoxLayout(spellSearchPanel, BoxLayout.X_AXIS)
		spellSearchPanel.add(JLabel("search:"))
		spellSearchPanel.add(spellSearchTextField)
		val spellBtnPanel = PaddedPanel()
		spellBtnPanel.add(spellAddBtn)
		spellPanel.add(spellSearchPanel, BorderLayout.NORTH)
		spellPanel.add(spellScrollPane, BorderLayout.CENTER)
		spellPanel.add(spellBtnPanel, BorderLayout.SOUTH)

		val perkSearchPanel = PaddedPanel()
		perkSearchPanel.layout = BoxLayout(perkSearchPanel, BoxLayout.X_AXIS)
		perkSearchPanel.add(JLabel("search:"))
		perkSearchPanel.add(perkSearchTextField)
		val perkBtnPanel = PaddedPanel()
		perkBtnPanel.add(perkAddBtn)
		perkPanel.add(perkSearchPanel, BorderLayout.NORTH)
		perkPanel.add(perkScrollPane, BorderLayout.CENTER)
		perkPanel.add(perkBtnPanel, BorderLayout.SOUTH)

		val specialSearchPanel = PaddedPanel()
		specialSearchPanel.layout = BoxLayout(specialSearchPanel, BoxLayout.X_AXIS)
		specialSearchPanel.add(JLabel("search:"))
		specialSearchPanel.add(specialSearchTextField)
		val specialBtnPanel = PaddedPanel()
		specialBtnPanel.add(specialAddBtn)
		specialPanel.add(specialSearchPanel, BorderLayout.NORTH)
		specialPanel.add(specialScrollPane, BorderLayout.CENTER)
		specialPanel.add(specialBtnPanel, BorderLayout.SOUTH)

		mainPanel.add(splitPane, BorderLayout.CENTER)

		matTable.setDefaultRenderer(Int::class.java, BiomeTableColorCellRenderer())
		matTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		entTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		entTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		spellTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		spellTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		perkTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		perkTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		specialTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		specialTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		matTableModel.setMaterials(noitaData.materials)
		entTableModel.setEntities(noitaData.entities)
		entTable.rowHeight = 32
		perkTableModel.setPerks(noitaData.perks)
		perkTable.rowHeight = 32
		spellTableModel.setSpells(noitaData.spells)
		spellTable.rowHeight = 32
		specialTableModel.setSpecials(noitaData.specials)
		specialTable.rowHeight = 32

		objPanel.layout = BorderLayout()
		objPanel.add(objScrollPane, BorderLayout.CENTER)
		val objBtnPanel = PaddedPanel()
		objBtnPanel.layout = BoxLayout(objBtnPanel, BoxLayout.X_AXIS)
		objBtnPanel.add(objDelBtn)
		objPanel.add(objBtnPanel, BorderLayout.SOUTH)

		matColorPanel.preferredSize = Dimension(EmUnit.EM_SIZE * 3, matColorPanel.preferredSize.height)
		matTextLabel.preferredSize = Dimension(EmUnit.EM_SIZE * 20, matTextLabel.preferredSize.height)
		matTextLabel.border = EmptyBorder(0, 5, 0 , 0)
		val selMatPanel = panelWithMult(matColorPanel, matTextLabel)
		selMatPanel.border = BevelBorder(BevelBorder.LOWERED)

		eyeColorPanel.preferredSize = Dimension(EmUnit.EM_SIZE * 3, matColorPanel.preferredSize.height)
		eyeTextLabel.preferredSize = Dimension(EmUnit.EM_SIZE * 20, matTextLabel.preferredSize.height)
		eyeTextLabel.border = EmptyBorder(0, 5, 0 , 0)
		val eyePanel = panelWithMult(eyeColorPanel, eyeTextLabel)
		eyePanel.border = BevelBorder(BevelBorder.LOWERED)

		val eastPanel = JPanel()
		eastPanel.layout = GridLayout(1, 2, 4, 0)
		eastPanel.border = EmptyBorder(0, 0, 0, 0)
		val rpanel = flowPanelWith(replaceBtn)
		rpanel.layout = FlowLayout(FlowLayout.TRAILING, 2, 2)
		eastPanel.add(rpanel)
		eastPanel.add(eyePanel)
		eastPanel.add(selMatPanel)

		tabPane.addTab("materials", matPanel)
		tabPane.addTab("entities", entPanel)
		tabPane.addTab("spells", spellPanel)
		tabPane.addTab("perks", perkPanel)
		tabPane.addTab("special", specialPanel)

		matSearchTextField.addActionListener { onMatSearchText(it) }
		entSearchTextField.addActionListener { onEntSearchText(it) }
		spellSearchTextField.addActionListener { onSpellSearchText(it) }
		perkSearchTextField.addActionListener { onPerkSearchText(it) }
		replaceBtn.addActionListener { onReplaceColor() }

		matTable.selectionModel.addListSelectionListener { onSelectMat(it) }

		entAddBtn.addActionListener { onAddEntity() }
		perkAddBtn.addActionListener { onAddPerk() }
		spellAddBtn.addActionListener { onAddSpell() }
		specialAddBtn.addActionListener { onAddSpecial() }

		val statPanel = JPanel()
		statPanel.layout = BorderLayout(2, 0)
		statPanel.border = EmptyBorder(0, 0, 0, 0)

		val posPanel = JPanel()
		imageXLabel.border = BevelBorder(BevelBorder.LOWERED)
		imageYLabel.border = BevelBorder(BevelBorder.LOWERED)
		imageXLabel.preferredSize = Dimension(60, imageXLabel.preferredSize.height)
		imageYLabel.preferredSize = Dimension(60, imageYLabel.preferredSize.height)
		posPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		posPanel.border = EmptyBorder(2, 5, 2, 5)
		posPanel.add(JLabel("pos - X: "))
		posPanel.add(imageXLabel)
		posPanel.add(JLabel(" Y: "))
		posPanel.add(imageYLabel)
		posPanel.add(imageColorLabel)
		statPanel.add(posPanel, BorderLayout.WEST)
		statPanel.add(eastPanel, BorderLayout.EAST)
		mainPanel.add(statPanel, BorderLayout.SOUTH)

		replaceBtn.isFocusable = false
		entAddBtn.isFocusable = false
		objDelBtn.isFocusable = false

		sceneDrawPanel.matMap = noitaData.materials.associateBy { it.rgb }
		sceneDrawPanel.entMap = noitaData.entities.associateBy { it.entity_filename }
		sceneDrawPanel.spellMap = noitaData.spells.associateBy { it.id }
		sceneDrawPanel.perkMap = noitaData.perks.associateBy { it.id }
		sceneDrawPanel.specialMap = noitaData.specials.associateBy { it.name }

		objTableModel.setObjects(scene.objects)
		sceneDrawPanel.setObjects(scene.objects)

		if (config.sceneSplitterPosition == -1) {
			config.sceneSplitterPosition = (frame.width / 4) * 3
		}
		splitPane.dividerLocation = config.sceneSplitterPosition
		if (config.sceneSideSplitterPosition == -1) {
			config.sceneSideSplitterPosition = (frame.height / 2)
		}
		splitPane.dividerLocation = config.sceneSplitterPosition
		sidePanel.dividerLocation = config.sceneSideSplitterPosition
		if (config.materialTableColumnWidths.isNotEmpty()) {
			config.materialTableColumnWidths.forEachIndexed { idx, width ->
				matTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		if (config.entityTableColumnWidths.isNotEmpty()) {
			config.entityTableColumnWidths.forEachIndexed { idx, width ->
				entTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		if (config.objectTableColumnWidths.isNotEmpty()) {
			config.objectTableColumnWidths.forEachIndexed { idx, width ->
				objTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		if (config.perkTableColumnWidths.isNotEmpty()) {
			config.perkTableColumnWidths.forEachIndexed { idx, width ->
				perkTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		if (config.spellTableColumnWidths.isNotEmpty()) {
			config.spellTableColumnWidths.forEachIndexed { idx, width ->
				spellTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		if (config.specialTableColumnWidths.isNotEmpty()) {
			config.specialTableColumnWidths.forEachIndexed { idx, width ->
				specialTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}

		frame.title = "Pixel Scene: ${scene.name}"
		frame.rootPane.registerKeyboardAction(::onUndo, KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW)
		frame.addComponentListener(this)
	}

	private fun onUndo(e: ActionEvent) {
		sceneDrawPanel.undo()
	}

	private fun onMatSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			matTableModel.setMaterials(noitaData.materials)
		} else {
			matTableModel.setMaterials(noitaData.materials.filter {
				it.ui_name.contains(txt, true) ||
					it.name.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

	private fun onEntSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			entTableModel.setEntities(noitaData.entities)
		} else {
			entTableModel.setEntities(noitaData.entities.filter {
				it.short_filename.contains(txt, true) ||
					it.name.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

	private fun onSpellSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			spellTableModel.setSpells(noitaData.spells)
		} else {
			spellTableModel.setSpells(noitaData.spells.filter {
				it.name.contains(txt, true) ||
					it.description.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

	private fun onPerkSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			perkTableModel.setPerks(noitaData.perks)
		} else {
			perkTableModel.setPerks(noitaData.perks.filter {
				it.ui_name.contains(txt, true) ||
					it.ui_description.contains(txt, true) ||
					it.english_name.contains(txt, true) ||
					it.english_desc.contains(txt, true)
			})
		}
	}

	private fun onSelectMat(e: ListSelectionEvent) {
		if (!e.valueIsAdjusting) {
			matTableModel.getMaterialAt(matTable.selectedRow)?.let { mat ->
				selectedMat = mat
				matColorPanel.background = Color(mat.rgb)
				matColorPanel.foreground = Color(mat.rgb)
				matTextLabel.text = mat.name
				sceneDrawPanel.paintColor = mat.rgb
			}
		}
	}

	private fun onAddEntity() {
		entTableModel.entityAt(entTable.selectedRow)?.let { ent ->
			val newId = (scene.objects.maxOfOrNull { it.id } ?: 0) + 1
			val newObj = SceneObj(
				id = newId,
				type = SceneObjType.ENTITY,
				resource_name = ent.entity_filename,
				short_name = ent.short_filename,
				x = 0,
				y = 0,
				w = ent.sprite_rect.w,
				h = ent.sprite_rect.h,
				x_offset = ent.sprite_offset_x,
				y_offset = ent.sprite_offset_y,
			)
			scene.objects.add(newObj)
			sceneDrawPanel.setObjects(scene.objects)
			objTableModel.setObjects(scene.objects)
		}
	}

	private fun onAddPerk() {
		perkTableModel.perkAt(perkTable.selectedRow)?.let { perk ->
			val newId = (scene.objects.maxOfOrNull { it.id } ?: 0) + 1
			val newObj = SceneObj(
				id = newId,
				type = SceneObjType.PERK,
				resource_name = perk.id,
				short_name = perk.english_name,
				x = 0,
				y = 0,
				w = perk.image?.width ?: 0,
				h = perk.image?.height ?: 0
			)
			scene.objects.add(newObj)
			sceneDrawPanel.setObjects(scene.objects)
			objTableModel.setObjects(scene.objects)
		}
	}

	private fun onAddSpell() {
		spellTableModel.spellAt(spellTable.selectedRow)?.let { spell ->
			val newId = (scene.objects.maxOfOrNull { it.id } ?: 0) + 1
			val newObj = SceneObj(
				id = newId,
				type = SceneObjType.SPELL,
				resource_name = spell.id,
				short_name = spell.english_name,
				x = 0,
				y = 0,
				w = spell.image?.width ?: 0,
				h = spell.image?.height ?: 0
			)
			scene.objects.add(newObj)
			sceneDrawPanel.setObjects(scene.objects)
			objTableModel.setObjects(scene.objects)
		}
	}

	private fun onAddSpecial() {
		specialTableModel.specialAt(specialTable.selectedRow)?.let { special ->
			when (special.cls) {
				PotionConfig::class -> doSpecialPotionDlg(special)
				ChestConfig::class -> doSpecialChestDlg(special)
				TeleportConfig::class -> doSpecialTeleDlg(special)
			}
		}
	}

	private fun doSpecialPotionDlg(special: Special) {
		SpecialPotionDlg(frame, "Potion with specific contents", config, noitaData.materials)
			.withMat("water")
			.showModal().let { (success, potionConfig) ->
				if (success && potionConfig != null) {
					val newId = (scene.objects.maxOfOrNull { it.id } ?: 0) + 1
					scene.objects.add(SceneObj(
						id = newId,
						type = SceneObjType.SPECIAL,
						resource_name = special.name,
						short_name = "${potionConfig.material} potion",
						x = 0,
						y = 0,
						w = special.image?.width ?: 0,
						h = special.image?.height ?: 0,
						x_offset = special.sprite_offset_x,
						y_offset = special.sprite_offset_y,
						config = potionConfig
					))
					objTableModel.setObjects(scene.objects)
				}
			}
	}

	private fun doSpecialChestDlg(special: Special) {
		SpecialChestDlg(frame, "Chest with specific contents", config, noitaData)
			.showModal().let { (success, chestConfig) ->
				if (success && chestConfig != null) {
					val newId = (scene.objects.maxOfOrNull { it.id } ?: 0) + 1
					scene.objects.add(SceneObj(
						id = newId,
						type = SceneObjType.SPECIAL,
						resource_name = special.name,
						short_name = "${chestConfig.resource_name} ${chestConfig.type.name} chest",
						x = 0,
						y = 0,
						w = special.image?.width ?: 0,
						h = special.image?.height ?: 0,
						x_offset = special.sprite_offset_x,
						y_offset = special.sprite_offset_y,
						config = chestConfig
					))
					objTableModel.setObjects(scene.objects)
				}
			}
	}

	private fun doSpecialTeleDlg(special: Special) {
		CoordinateDlg(frame, "Teleport to", 0, 0, project)
			.showModal().let { (success, coords) ->
				if (success && coords != null) {
					val newId = (scene.objects.maxOfOrNull { it.id } ?: 0) + 1
					scene.objects.add(SceneObj(
						id = newId,
						type = SceneObjType.SPECIAL,
						resource_name = special.name,
						short_name = "${special.name} to ${coords}",
						x = 0,
						y = 0,
						w = special.image?.width ?: 0,
						h = special.image?.height ?: 0,
						x_offset = special.sprite_offset_x,
						y_offset = special.sprite_offset_y,
						config = TeleportConfig(coords.first, coords.second)
					))
				}
			}
	}

	private fun onReplaceColor() {
		val replColor = replacementColor ?: return
		sceneDrawPanel.replaceColor(replColor, sceneDrawPanel.paintColor)
	}

	override fun showStatus(x: Int, y: Int, msg: String) {
		imageXLabel.text = "$x"
		imageYLabel.text = "$y"
		imageColorLabel.text = msg
	}

	override fun objectPosChanged(obj: SceneObj) {
		objTableModel.setObjects(scene.objects)
	}

	override fun replacementColorSelected(color: Int) {
		replacementColor = color
		eyeColorPanel.foreground = Color(color)
		eyeColorPanel.background = Color(color)
		eyeTextLabel.text = String.format("%08x", color.toLong())?.let {
			if (it.length > 8) it.substring((it.length-8)) else it
		}
	}

	override fun componentShown(evt: ComponentEvent) {
		println("componentShown")
		loadImages()
	}

	override fun componentResized(evt: ComponentEvent) {
		println("scene form resized")
	}

	fun getVerticalDividerLocation() = splitPane.dividerLocation

	fun getHorizontalDividerLocation() = sidePanel.dividerLocation

	fun getMaterialTableColumnWidths() = (0 until 3).map {
		matTable.columnModel.getColumn(it).preferredWidth
	}

	fun getEntityTableColumnWidths() = (0 until 3).map {
		entTable.columnModel.getColumn(it).preferredWidth
	}

	fun getObjectTableColumnWidths() = (0 until 5).map {
		objTable.columnModel.getColumn(it).preferredWidth
	}

	fun getPerkTableColumnWidths() = (0 until 3).map {
		perkTable.columnModel.getColumn(it).preferredWidth
	}

	fun getSpellTableColumnWidths() = (0 until 3).map {
		spellTable.columnModel.getColumn(it).preferredWidth
	}

	fun getSpecialTableColumnWidths() = (0 until 3).map {
		specialTable.columnModel.getColumn(it).preferredWidth
	}

	private fun loadImages() {
		val projFolder = getProjectFolder(config, project)
		val imgFolder = File(projFolder, "files/pixel_scenes")
		val bkgImage = ImageIO.read(File(imgFolder, "${scene.name}_background.png"))
		val matImage = ImageIO.read(File(imgFolder, "${scene.name}_material.png"))
		val visImage = ImageIO.read(File(imgFolder, "${scene.name}_visual.png"))
		sceneDrawPanel.setImages(matImage, visImage, bkgImage)
		sceneDrawPanel.fitImage()
	}

	fun saveImages() {
		val projFolder = getProjectFolder(config, project)
		val imgFolder = File(projFolder, "files/pixel_scenes")
		ImageIO.write(sceneDrawPanel.getBkgImg(), "png", File(imgFolder, "${scene.name}_background.png"))
		ImageIO.write(sceneDrawPanel.getMatImg(), "png", File(imgFolder, "${scene.name}_material.png"))
		ImageIO.write(sceneDrawPanel.getVisImg(), "png", File(imgFolder, "${scene.name}_visual.png"))
	}


}