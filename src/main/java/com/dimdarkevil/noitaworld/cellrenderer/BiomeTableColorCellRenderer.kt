package com.dimdarkevil.noitaworld.cellrenderer

import java.awt.Color
import java.awt.Component
import javax.swing.JLabel
import javax.swing.JTable
import javax.swing.table.DefaultTableCellRenderer

class BiomeTableColorCellRenderer : DefaultTableCellRenderer() {
	private val label = JLabel()

	init {
		label.isOpaque = true
	}

	override fun getTableCellRendererComponent(table: JTable, value: Any, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
		label.background = Color(value as Int)
		return label
	}

}