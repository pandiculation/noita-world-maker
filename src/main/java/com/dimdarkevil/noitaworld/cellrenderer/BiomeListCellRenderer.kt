package com.dimdarkevil.noitaworld.cellrenderer

import com.dimdarkevil.noitaworld.ColorIcon
import com.dimdarkevil.noitaworld.model.Biome
import java.awt.Color
import java.awt.Component
import javax.swing.DefaultListCellRenderer
import javax.swing.JLabel
import javax.swing.JList
import javax.swing.UIManager

class BiomeListCellRenderer : DefaultListCellRenderer() {
	private val label = JLabel()
	private val bgClr = UIManager.getDefaults()["List.background"] as Color
	private val fgClr = UIManager.getDefaults()["List.foreground"] as Color
	private val selBgClr = UIManager.getDefaults()["List.selectionBackground"] as Color
	private val selFgClr = UIManager.getDefaults()["List.selectionForeground"] as Color

	init {
		label.isOpaque = true
	}

	override fun getListCellRendererComponent(
		list: JList<*>,
		value: Any,
		index: Int,
		isSelected: Boolean,
		cellHasFocus: Boolean
	): Component {
		val biome = value as Biome
		label.icon = ColorIcon(biome.rgb, 32, 32)
		label.text = "${biome.short_filename} (${biome.english_name})"
		if (isSelected) {
			label.background = selBgClr
			label.foreground = selFgClr
		} else {
			label.background = bgClr
			label.foreground = fgClr
		}
		return label
	}

}