package com.dimdarkevil.noitaworld.cellrenderer

import com.dimdarkevil.noitaworld.EntityIconPanel
import java.awt.Color
import java.awt.Component
import java.awt.image.BufferedImage
import javax.swing.JTable
import javax.swing.table.DefaultTableCellRenderer

class EntityTableImgCellRenderer(itemHeight: Int) : DefaultTableCellRenderer() {
	private val panel = EntityIconPanel(itemHeight)

	override fun getTableCellRendererComponent(table: JTable, value: Any, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
		val img = value as BufferedImage
		panel.setImage(img)
		return panel
	}
}