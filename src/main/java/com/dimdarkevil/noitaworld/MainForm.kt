package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.io.NoitaData
import com.dimdarkevil.noitaworld.listener.MapStatusListener
import com.dimdarkevil.noitaworld.model.*
import com.dimdarkevil.noitaworld.cellrenderer.BiomeTableColorCellRenderer
import com.dimdarkevil.noitaworld.tablemodel.BiomeTableModel
import com.dimdarkevil.noitaworld.tablemodel.SceneTableModel
import com.dimdarkevil.swingutil.*
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.readValue
import java.awt.*
import java.awt.event.*
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileOutputStream
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.border.BevelBorder
import javax.swing.border.EmptyBorder
import javax.swing.event.ListSelectionEvent
import javax.swing.filechooser.FileNameExtensionFilter
import kotlin.RuntimeException

class MainForm(private val config: AppConfig, private val frame: JFrame, private val noitaData: NoitaData) : ComponentListenerAdapter, MapStatusListener {
	val mainPanel = JPanel()
	private val toolBar = JToolBar()
	private val newBtn = JButton("new")
	private val openBtn = JButton("open")
	private val saveBtn = JButton("save")
	//private val settingsBtn = JButton("settings")
	private val exitBtn = JButton("exit")
	private val mapDrawPanel = MapDrawPanel(this)

	private val biomePanel = JPanel(BorderLayout())
	private val biomeSearchTextField = JTextField()
	private val biomeTableModel = BiomeTableModel()
	private val biomeTable = JTable(biomeTableModel)
	private val biomeScrollPane = JScrollPane(biomeTable)

	private val scenePanel = JPanel(BorderLayout())
	private val sceneTableModel = SceneTableModel()

	private val sceneTable = JTable(sceneTableModel)
	private val sceneScrollPane = JScrollPane(sceneTable)
	private val sceneNewBtn = JButton("new scene")
	private val sceneEditBtn = JButton("edit scene")
	private val sceneDelBtn = JButton("delete scene")

	private val sidePane = JSplitPane(JSplitPane.VERTICAL_SPLIT, biomePanel, scenePanel)

	private val splitPane = JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mapDrawPanel, sidePane)
	private val allBtns = mutableListOf<JButton>()

	private val worldXLabel = JLabel("0")
	private val worldYLabel = JLabel("0")
	private val blockXLabel = JLabel("0")
	private val blockYLabel = JLabel("0")
	private val biomeLabel = JLabel("")

	private val biomeColorPanel = JPanel()
	private val biomeTextLabel = JLabel("")

/*
	private val drawBtn = JRadioButton("draw")
	private val moveBtn = JRadioButton("move")
	private val modeBtnGroup = ButtonGroup()
*/

/*
	private var translations = mapOf<String,String>()
	private var biomes : List<Biome> = listOf()
	private var materials: List<Material> = listOf()
	private var entities: List<Entity> = listOf()
*/
	private var lastDir: File? = null
	private var project: Project? = null

	init {
		mainPanel.layout = BorderLayout(0, 0)
		mainPanel.add(toolBar, BorderLayout.NORTH)
		toolBar.isFloatable = false
		//toolBar.isRollover = true
		mainPanel.add(splitPane, BorderLayout.CENTER)

		val biomeSearchPanel = PaddedPanel()
		biomeSearchPanel.layout = BoxLayout(biomeSearchPanel, BoxLayout.X_AXIS)
		biomeSearchPanel.add(JLabel("search:"))
		biomeSearchPanel.add(biomeSearchTextField)
		biomePanel.add(biomeSearchPanel, BorderLayout.NORTH)
		biomePanel.add(biomeScrollPane, BorderLayout.CENTER)

		val sceneBtnPanel = PaddedPanel()
		sceneBtnPanel.layout = BoxLayout(sceneBtnPanel, BoxLayout.X_AXIS)
		sceneBtnPanel.add(sceneNewBtn)
		sceneBtnPanel.add(sceneEditBtn)
		sceneBtnPanel.add(sceneDelBtn)
		scenePanel.add(sceneBtnPanel, BorderLayout.SOUTH)
		scenePanel.add(sceneScrollPane, BorderLayout.CENTER)

		val statPanel = JPanel()
		statPanel.layout = BorderLayout(2, 2)
		val posPanel = JPanel()
		biomeLabel.border = EmptyBorder(0, 5, 0, 0)
		worldXLabel.border = BevelBorder(BevelBorder.LOWERED)
		worldYLabel.border = BevelBorder(BevelBorder.LOWERED)
		blockXLabel.border = BevelBorder(BevelBorder.LOWERED)
		blockYLabel.border = BevelBorder(BevelBorder.LOWERED)
		worldXLabel.preferredSize = Dimension(60, worldXLabel.preferredSize.height)
		worldYLabel.preferredSize = Dimension(60, worldYLabel.preferredSize.height)
		blockXLabel.preferredSize = Dimension(60, blockXLabel.preferredSize.height)
		blockYLabel.preferredSize = Dimension(60, blockYLabel.preferredSize.height)
		posPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		posPanel.border = EmptyBorder(2, 5, 2, 5)
		posPanel.add(JLabel("world pos - X: "))
		posPanel.add(worldXLabel)
		posPanel.add(JLabel(" Y: "))
		posPanel.add(worldYLabel)
		posPanel.add(JLabel("block pos - X: "))
		posPanel.add(blockXLabel)
		posPanel.add(JLabel(" Y: "))
		posPanel.add(blockYLabel)
		posPanel.add(biomeLabel)
		statPanel.add(posPanel, BorderLayout.WEST)

		biomeColorPanel.preferredSize = Dimension(EmUnit.EM_SIZE * 3, biomeColorPanel.preferredSize.height)
		biomeTextLabel.preferredSize = Dimension(EmUnit.EM_SIZE * 20, biomeTextLabel.preferredSize.height)
		//matColorLabel.border = BevelBorder(BevelBorder.LOWERED)
		//matTextLabel.border = BevelBorder(BevelBorder.LOWERED)
		biomeTextLabel.border = EmptyBorder(0, 5, 0 , 0)
		val selBiomePanel = panelWithMult(biomeColorPanel, biomeTextLabel)
		selBiomePanel.border = BevelBorder(BevelBorder.LOWERED)
		statPanel.add(selBiomePanel, BorderLayout.EAST)

/*
		val modePanel = JPanel()
		drawBtn.isFocusable = false
		moveBtn.isFocusable = false
		modeBtnGroup.add(drawBtn)
		modeBtnGroup.add(moveBtn)
		modePanel.add(JLabel("mode: "))
		modePanel.add(drawBtn)
		modePanel.add(moveBtn)
		statPanel.add(modePanel, BorderLayout.EAST)
		drawBtn.isSelected = true
*/

		mainPanel.add(statPanel, BorderLayout.SOUTH)

		newBtn.icon = IconLoader.imgIcon(Ico.NEW, 32)
		openBtn.icon = IconLoader.imgIcon(Ico.OPEN, 32)
		saveBtn.icon = IconLoader.imgIcon(Ico.SAVE, 32)
		//settingsBtn.icon = IconLoader.imgIcon(Ico.RUN, 32)
		exitBtn.icon = IconLoader.imgIcon(Ico.EXIT, 32)
		addBtn(newBtn)
		addBtn(openBtn)
		addBtn(saveBtn)
		toolBar.add(Box.createHorizontalGlue())
		//addBtn(settingsBtn)
		addBtn(exitBtn)
		setupBtnStyles(allBtns)

		biomeTable.setDefaultRenderer(Int::class.java, BiomeTableColorCellRenderer())
		biomeTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION
		sceneTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		openBtn.addActionListener { onOpen() }
		newBtn.addActionListener { onNew() }
		saveBtn.addActionListener { onSave() }
		//settingsBtn.addActionListener { showSettingsDlg() }
		exitBtn.addActionListener { onExit() }
		biomeSearchTextField.addActionListener { onBiomeSearchText(it) }
		biomeTable.selectionModel.addListSelectionListener { onBiomeSelected(it) }
/*
		drawBtn.addActionListener { onModeChanged() }
		moveBtn.addActionListener { onModeChanged() }
*/

		sceneTable.addMouseListener(object: MouseAdapter() {
			override fun mousePressed(e: MouseEvent) {
				val table = e.source as JTable
				val row = table.rowAtPoint(e.point)
				if (e.clickCount == 2 && table.selectedRow >= 0) {
					onChangeSceneCoords(row)
				}
			}
		})


		sceneNewBtn.addActionListener { onNewScene() }
		sceneEditBtn.addActionListener { createSceneForm() }
		frame.rootPane.registerKeyboardAction(::onUndo, KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW)
		frame.addComponentListener(this)
	}

/*
	fun onModeChanged() {
		if (drawBtn.isSelected) {
			mapDrawPanel.setMode(DrawMode.DRAW)
		} else {
			mapDrawPanel.setMode(DrawMode.MOVE)
		}
	}
*/

	fun onUndo(e: ActionEvent) {
		mapDrawPanel.undo()
	}

	fun onBiomeSelected(e: ListSelectionEvent) {
		project ?: return
		//println("Biome selected: ${selectedBiome.short_filename} ${e.firstIndex} ${e.lastIndex} ${e.valueIsAdjusting}")
		if (!e.valueIsAdjusting) {
			biomeTableModel.getBiomeAt(biomeTable.selectedRow)?.let { selectedBiome ->
				mapDrawPanel.paintColor = selectedBiome.rgb
				biomeColorPanel.foreground = Color(selectedBiome.rgb)
				biomeColorPanel.background = Color(selectedBiome.rgb)
				biomeTextLabel.text = selectedBiome.short_filename
			}
		}
	}

	private fun addBtn(btn: JButton) {
		toolBar.add(btn)
		allBtns.add(btn)
	}

	private fun onOpen() {
		val fc = JFileChooser()
		fc.currentDirectory = File(File(config.noitaExeFile).parentFile, "mods")
		fc.isMultiSelectionEnabled = false
		fc.fileFilter = FileNameExtensionFilter("JSON files", "json")
		val res = fc.showOpenDialog(frame)
		if (res != JFileChooser.APPROVE_OPTION) return
		val f = fc.selectedFile ?: return
		lastDir = f.parentFile
		val proj = ObjectMapper().readValue<Project>(f)
		val bi = ImageIO.read(File(f.parentFile, "files/biome_map.png"))
		mapDrawPanel.setMap(bi, proj.biome_map.startY, proj.playerStartX, proj.playerStartY)
		mapDrawPanel.fitImage()
		mapDrawPanel.setPixelScenes(proj.pixel_scenes)
		proj.biome_map.bi = bi
		sceneTableModel.setScenes(proj.pixel_scenes)
		project = proj
	}

	private fun onNew() {
		NewMapDlg(frame, "new map").showModal().let { (success, proj) ->
			if (success && proj != null) {
				project = proj
				createProject()
				val bi = proj.biome_map.bi ?: throw RuntimeException("biome map not created")
				mapDrawPanel.setMap(bi, proj.biome_map.startY, proj.playerStartX, proj.playerStartY)
			}
		}
	}

	private fun onSave() {
		if (save()) showInfo("saved", "Project saved")
	}

	private fun save() : Boolean {
		val p = project ?: return false
		val projFolder = getProjectFolder(config, p)
		val filesFolder = File(projFolder, "files")
		ImageIO.write(p.biome_map.bi, "png", File(filesFolder, "biome_map.png"))
		val projectFile = File(projFolder, "noita-world-maker.json")
		ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValue(projectFile, p)

		// save the pixel scenes
		val psFolder = "mods/${p.name}/files/pixel_scenes"
		val eFolder = "mods/${p.name}/files/entities"
		val initLua = File(projFolder, "init.lua").readLines()
		var inPixelScenes = false
		var inEntities = false
		val newInitLua = mutableListOf<String>()
		val chestSpecials = mutableListOf<Pair<String,ChestConfig>>()
		initLua.forEach { line ->
			val l = line.trim()
			when {
				inPixelScenes -> {
					if (l == "--load_pixel_scenes_end") {
						newInitLua.add(line)
						inPixelScenes = false
					}
				}
				inEntities -> {
					if (l == "--load_entities_end") {
						newInitLua.add(line)
						inEntities = false
					}
				}
				else -> {
					when (l) {
						"--load_entities_start" -> {
							newInitLua.add(line)
							var potionIdx = 0
							var chestIdx = 0
							var teleIdx = 0
							p.pixel_scenes.forEach { ps ->
								ps.objects.forEach { o ->
									val ox = o.x + ps.x + o.x_offset
									val oy = o.y + ps.y + o.y_offset
									when (o.type) {
										SceneObjType.ENTITY -> newInitLua.add("""  EntityLoad("${o.resource_name}", ${ox}, ${oy})""")
										SceneObjType.PERK -> newInitLua.add("""  perk_spawn( ${ox}, ${oy}, "${o.resource_name}", true )""")
										SceneObjType.SPELL -> newInitLua.add("""  CreateItemActionEntity( "${o.resource_name}", ${ox}, ${oy} )""")
										SceneObjType.SPECIAL -> {
											o.config?.let { soconfig ->
												when (soconfig) {
													is PotionConfig -> {
														potionIdx++
														newInitLua.add("""  local potion_special_${potionIdx} = EntityLoad("data/entities/items/pickup/potion_empty.xml", ${ox}, ${oy})""")
														newInitLua.add("""  init_potion( potion_special_${potionIdx}, "${soconfig.material}" )""")
													}
													is ChestConfig -> {
														chestIdx++
														newInitLua.add("""  local chest_special_${chestIdx} = EntityLoad("mods/${p.name}/files/entities/chest_specific.xml", ${ox}, ${oy})""")
														newInitLua.add("""  EntitySetName( chest_special_${chestIdx}, "cs_${chestIdx}" )""")
														chestSpecials.add(Pair("cs_${chestIdx}", soconfig))
													}
													is TeleportConfig -> {
														teleIdx++
														newInitLua.add("""  local tele_special_${chestIdx} = EntityLoad("mods/${p.name}/files/entities/teleport_to_location.xml", ${ox}, ${oy})""")
														newInitLua.add("""  EntitySetName( tele_special_${chestIdx}, "tele${chestIdx}_${soconfig.remote_x}_${soconfig.remote_y}" )""")
													}
												}
											}
										}
									}
								}
							}
							inEntities = true
						}
						"--load_pixel_scenes_start" -> {
							newInitLua.add(line)
							p.pixel_scenes.forEach { ps ->
								newInitLua.add("""  LoadPixelScene("${psFolder}/${ps.name}_material.png", "${psFolder}/${ps.name}_visual.png", ${ps.x}, ${ps.y}, "${psFolder}/${ps.name}_background.png", true)""")
							}
							inPixelScenes = true
						}
						else -> {
							if (l.startsWith("local playerX, playerY")) {
								newInitLua.add("local playerX, playerY = ${p.playerStartX}, ${p.playerStartY}")
							} else {
								newInitLua.add(line)
							}
						}
					}
				}
			}
		}
		File(projFolder, "init.lua").writeText(newInitLua.joinToString("\n"))

		val chestFile = File(getProjectFolder(config, p), "files/scripts/chest_spawns.lua")
		val chestFileContents = mutableListOf<String>()
		chestFileContents.add("""dofile_once("data/scripts/lib/utilities.lua")""")
		chestFileContents.add("""dofile_once("data/scripts/items/init_potion.lua")""")
		chestFileContents.add("""dofile_once("data/scripts/perks/perk.lua")""")
		chestFileContents.add("")
		if (chestSpecials.isNotEmpty()) {
			val funcList = chestSpecials.joinToString(",") { (name, _) ->
				"$name = ${name}_spawn"
			}
			chestFileContents.add("""chest_spawn_funcs = { ${funcList} }""")
			chestSpecials.forEach { (name, cconfig) ->
				chestFileContents.add("")
				chestFileContents.add("function ${name}_spawn(x, y)")
				when (cconfig.type) {
					SceneObjType.ENTITY -> chestFileContents.add("""  EntityLoad("${cconfig.resource_name}", x, y)""")
					SceneObjType.PERK -> chestFileContents.add("""  perk_spawn( x, y, "${cconfig.resource_name}", true )""")
					SceneObjType.SPELL -> chestFileContents.add("""  CreateItemActionEntity( "${cconfig.resource_name}", x, y )""")
					else -> chestFileContents.add("  -- could not spawn entity of type ${cconfig.type.name}")
				}
				chestFileContents.add("end")
			}
		} else {
			chestFileContents.add("""chest_spawn_funcs = {  }""")
		}
		chestFile.writeText(chestFileContents.joinToString("\n"), Charsets.UTF_8)
		/*
		val psScenes = p.pixel_scenes.map { ps ->
			"""
			|<PixelScene DEBUG_RELOAD_ME="0" background_filename="${psFolder}/${ps.name}_background.png" clean_area_before="0" colors_filename="${psFolder}/${ps.name}_visual.png" material_filename="${psFolder}/${ps.name}_material.png" pos_x="${ps.x}" pos_y="${ps.y}" skip_biome_checks="1" skip_edge_textures="0" >
			|</PixelScene>
			""".trimMargin("|")
		}.joinToString("\n")
		val psFile = "<PixelScenes>\n<mBufferedPixelScenes>\n${psScenes}\n</mBufferedPixelScenes>\n</PixelScenes>"
		*/

		val psFile = "<PixelScenes>\n</PixelScenes>"
		val f = File(getProjectFolder(config, p), "data/biome/_pixel_scenes.xml")
		f.writeText(psFile, Charsets.UTF_8)

		return true
	}

/*
	private fun onBiomeSelect(listSelEvent: ListSelectionEvent) {
		if (!listSelEvent.valueIsAdjusting) {
			val b = biomeListModel[biomeList.selectedIndex]
			//println("selected ${biomeList.selectedIndex}: ${b.short_filename} ${b.rgb}")
			mapPanel.paintColor = b.rgb
		}
	}
*/

	private fun onBiomeSearchText(e: ActionEvent) {
		//println(e.actionCommand)
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			biomeTableModel.setBiomes(noitaData.biomes)
		} else {
			biomeTableModel.setBiomes(noitaData.biomes.filter {
				it.biome_filename.contains(txt, true) ||
					it.name.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

	private fun onExit() {
		frame.dispatchEvent(WindowEvent(frame, WindowEvent.WINDOW_CLOSING))
	}

	private fun onNewScene() {
		val p = project ?: return
		val (success, scene) = NewSceneDlg(frame, "New Scene", config, p).showModal()
		if (success && scene != null) {
			p.pixel_scenes.add(scene)
			sceneTableModel.setScenes(p.pixel_scenes)
			mapDrawPanel.setPixelScenes(p.pixel_scenes)
			save()
		}
	}

	private fun onChangeSceneCoords(row: Int) {
		val p = project ?: return
		val scene = sceneTableModel.getSceneAt(row) ?: return
		CoordinateDlg(frame, "Change coords for scene ${scene.name}", scene.x, scene.y, p).showModal().let { (success, coords) ->
			if (success && coords != null) {
				scene.x = coords.first
				scene.y = coords.second
				sceneTableModel.setScenes(p.pixel_scenes)
			}
		}
	}

	override fun componentShown(evt: ComponentEvent) {
		println("-=-= componentShown")
		setupBtnSizes(allBtns)
		if (config.splitterPosition == -1) {
			config.splitterPosition = (frame.width / 4) * 3;
		}
		splitPane.dividerLocation = config.splitterPosition
		if (config.sideSplitterPosition == -1) {
			config.sideSplitterPosition = (frame.height / 3) * 2;
		}
		sidePane.dividerLocation = config.sideSplitterPosition
		if (config.biomeTableColumnWidths.isNotEmpty()) {
			config.biomeTableColumnWidths.forEachIndexed { idx, width ->
				biomeTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		if (config.sceneTableColumnWidths.isNotEmpty()) {
			config.sceneTableColumnWidths.forEachIndexed { idx, width ->
				sceneTable.columnModel.getColumn(idx).preferredWidth = width
			}
		}
		setBiomeListItems()
/*
		try {
			loadTranslations()
			loadBiomesAll()
			loadMaterials()
			loadEntities()
			setBiomeListItems()
		} catch (e: Exception) {
			showError("${e.message}")
			showSettingsDlg()
		}
*/
	}

	override fun componentResized(evt: ComponentEvent) {
		//println("-=-= componentResized")
	}

	private fun showError(msg: String) {
		JOptionPane.showMessageDialog(frame, msg, "Error", JOptionPane.ERROR_MESSAGE)
	}

	private fun showInfo(msg: String, title: String = "Info") {
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.INFORMATION_MESSAGE)
	}

	override fun showStatus(wx: Int, wy: Int, bx: Int, by: Int, msg: String) {
		worldXLabel.text = "$wx"
		worldYLabel.text = "$wy"
		blockXLabel.text = "$bx"
		blockYLabel.text = "$by"
		biomeLabel.text = msg
	}

	override fun scenePosChanged(scene: Scene) {
		val proj = project ?: return
		sceneTableModel.setScenes(proj.pixel_scenes)
	}

	override fun playerPosChangeed(px: Int, py: Int) {
		val proj = project ?: return
		proj.playerStartX = px
		proj.playerStartY = py
	}

	private fun setupBtnStyles(btns: List<JButton>) {
		println("-=-= setupBtnStyles")
		btns.forEach { btn ->
			btn.horizontalTextPosition = SwingConstants.CENTER
			btn.verticalTextPosition = SwingConstants.BOTTOM
		}
	}

	private fun setupBtnSizes(btns: List<JButton>) {
		println("-=-= setupBtnSizes")
		var maxWidth = 0
		var maxHeight = 0
		val margin = 4
		btns.forEach { btn ->
			if (btn.width > maxWidth) maxWidth = btn.width
			if (btn.height > maxHeight) maxHeight = btn.height
		}
		var x = margin
		btns.forEach { btn ->
			btn.setLocation(x, btn.y)
			btn.setSize(maxWidth, maxHeight)
			val size = Dimension(maxWidth, maxHeight)
			btn.minimumSize = size
			btn.maximumSize = size
			btn.preferredSize = size
			x += maxWidth + margin
		}
		toolBar.doLayout()
	}

	private fun showSettingsDlg() {
/*
		SettingsDlg(config, frame, "settings").showModal().let { (success, files) ->
			if (success && files != null) {
				val noitaSaveFolder = files.first
				val noitaExeFile = files.second
				if (noitaExeFile != config.noitaExeFile) {
					config.noitaExeFile = noitaExeFile
					loadTranslations()
				}
				if (noitaSaveFolder != config.noitaSaveFolder) {
					config.noitaSaveFolder = noitaSaveFolder
					loadBiomesAll()
					setBiomeListItems()
					loadMaterials()
					loadEntities()
				}
			}
		}
*/
	}

	private fun createSceneForm() {
		val proj = project ?: return
		val scene = sceneTableModel.getSceneAt(sceneTable.selectedRow) ?: return
		val sframe = JFrame("new scene")
		val sf = SceneForm(sframe, config, proj, scene, noitaData)
		sframe.contentPane = sf.mainPanel

		val screenSize = Toolkit.getDefaultToolkit().screenSize
		if (config.sceneWinWidth < 0) config.sceneWinWidth = 1600
		if (config.sceneWinHeight < 0) config.sceneWinHeight = 1000
		if (config.sceneWinX < 0) config.winX = (screenSize.width - config.sceneWinWidth) / 2
		if (config.sceneWinY < 0) config.winY = (screenSize.height - config.sceneWinHeight) / 2

		sframe.setSize(config.sceneWinWidth, config.sceneWinHeight)
		sframe.setLocationRelativeTo(null)
		sframe.setLocation(config.sceneWinX, config.sceneWinY)
		sframe.defaultCloseOperation = JFrame.DO_NOTHING_ON_CLOSE
		sframe.addWindowListener(object: WindowAdapter() {
			override fun windowClosing(e: WindowEvent) {
				// TODO: save the images
				println("-=-= sframe closing")
				sf.saveImages()
				updateSceneFormConfig(
					sframe,
					sf.getVerticalDividerLocation(),
					sf.getHorizontalDividerLocation(),
					sf.getMaterialTableColumnWidths(),
					sf.getEntityTableColumnWidths(),
					sf.getObjectTableColumnWidths(),
					sf.getPerkTableColumnWidths(),
					sf.getSpellTableColumnWidths(),
					sf.getSpecialTableColumnWidths()
				)
				sframe.dispose()
			}
		})
		EventQueue.invokeLater {
			sframe.isVisible = true
			println("made scene form visible")
		}
	}

	private fun setBiomeListItems() {
		biomeTableModel.setBiomes(noitaData.biomes)
		mapDrawPanel.biomeMap = noitaData.biomes.associateBy { it.rgb }
	}

	fun updateSceneFormConfig(
		sframe: JFrame,
		vertDividerLoc: Int,
		horzDividerLoc: Int,
		matColWidths: List<Int>,
		entColWidths: List<Int>,
		objColWidths: List<Int>,
		perkColWidths: List<Int>,
		spellColWidths: List<Int>,
		specialColWidths: List<Int>
	) {
		config.sceneWinX = sframe.x
		config.sceneWinY = sframe.y
		config.sceneWinWidth = sframe.width
		config.sceneWinHeight = sframe.height
		config.sceneSplitterPosition = vertDividerLoc
		config.sceneSideSplitterPosition = horzDividerLoc
		config.materialTableColumnWidths = matColWidths
		config.entityTableColumnWidths = entColWidths
		config.objectTableColumnWidths = objColWidths
		config.perkTableColumnWidths = perkColWidths
		config.spellTableColumnWidths = spellColWidths
		config.specialTableColumnWidths = specialColWidths
	}

	fun updateConfig() {
		config.winX = frame.x
		config.winY = frame.y
		config.winWidth = frame.width
		config.winHeight = frame.height
		config.splitterPosition = splitPane.dividerLocation
		config.sideSplitterPosition = sidePane.dividerLocation
		config.biomeTableColumnWidths = (0 until 3).map {
			biomeTable.columnModel.getColumn(it).preferredWidth
		}
		config.sceneTableColumnWidths = (0 until 5).map {
			sceneTable.columnModel.getColumn(it).preferredWidth
		}
	}

	fun loadTemplate(p: Project, resPath: String) : List<String> {
		// "data/biome/_pixel_scenes.xml"
		return javaClass.classLoader.getResourceAsStream(resPath)?.use { sin ->
			String(sin.readAllBytes(), Charsets.UTF_8).split("\n").map { line ->
				line.replace("%modname%", p.name)
					.replace("%moddesc%", p.description)
					.replace("%worldname%", p.pretty_name)
					.replace("%worlddesc%", p.pretty_description)
					.replace("%playerx%", "${p.playerStartX}")
					.replace("%playery%", "${p.playerStartY}")
					.replace("%biome_offset_y%", "${p.biome_map.startY}")
			}
		} ?: emptyList()
	}

	fun copyBinResource(resPath: String, destFile: File) {
		javaClass.classLoader.getResourceAsStream(resPath)?.use { sin ->
			FileOutputStream(destFile).use { fout ->
				fout.write(sin.readAllBytes())
			}
		}
	}

	fun copyTextResource(p: Project, resPath: String, destFile: File) {
		val lines = loadTemplate(p, resPath)
		destFile.writeText(lines.joinToString("\n"), Charsets.UTF_8)
	}

	fun createProject() {
		val p = project ?: return
		val projFolder = getProjectFolder(config, p)
		val biomeFolder = File(projFolder, "data/biome")
		val filesFolder = File(projFolder, "files")
		val entitiesFolder = File(filesFolder, "entities")
		val scriptsFolder = File(filesFolder, "scripts")
		entitiesFolder.mkdirs()
		scriptsFolder.mkdirs()
		biomeFolder.mkdirs()

		copyTextResource(p, "mod/mod.xml", File(projFolder, "mod.xml"))
		copyTextResource(p, "mod/init.lua", File(projFolder, "init.lua"))
		copyTextResource(p, "mod/data/biome/_biomes_all.xml", File(biomeFolder, "_biomes_all.xml"))
		copyTextResource(p, "mod/data/biome/_pixel_scenes.xml", File(biomeFolder, "_pixel_scenes.xml"))
		copyTextResource(p, "mod/files/magic_numbers.xml", File(filesFolder, "magic_numbers.xml"))
		copyTextResource(p, "mod/files/entities/chest_specific.xml", File(entitiesFolder, "chest_specific.xml"))
		copyTextResource(p, "mod/files/scripts/chest_specific.lua", File(scriptsFolder, "chest_specific.lua"))
		copyTextResource(p, "mod/files/entities/teleport_to_location.xml", File(entitiesFolder, "teleport_to_location.xml"))
		copyTextResource(p, "mod/files/scripts/teleport_to_location.lua", File(scriptsFolder, "teleport_to_location.lua"))
		copyTextResource(p, "mod/files/scripts/util.lua", File(scriptsFolder, "util.lua"))
		copyTextResource(p, "mod/files/scripts/chest_spawns.lua", File(scriptsFolder, "chest_spawns.lua"))
		copyBinResource("mod/files/menu_banner_background.png", File(filesFolder, "menu_banner_background.png"))
		val overlay = ImageIO.read(javaClass.classLoader.getResourceAsStream("mod/files/menu_banner_overlay.png"))
		drawProjectName(overlay, p.pretty_name)
		ImageIO.write(overlay, "png", File(filesFolder, "menu_banner_overlay.png"))
		ImageIO.write(p.biome_map.bi, "png", File(filesFolder, "biome_map.png"))
		val projectFile = File(projFolder, "noita-world-maker.json")
		ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValue(projectFile, p)
	}

	fun drawProjectName(bi: BufferedImage, projName: String) {
		val g = bi.createGraphics()
		g.color = Color("ff9aabff".toLong(16).toInt())
		var fontSize = 40f
		var font = g.font.deriveFont(fontSize)
		var fm = g.getFontMetrics(font)
		while (fm.stringWidth(projName) > bi.width || fm.height > bi.height) {
			fontSize -= 1f
			font = font.deriveFont(fontSize)
			fm = g.getFontMetrics(font)
		}
		g.font = font
		val x = (bi.width - fm.stringWidth(projName)) / 2
		val y = ((bi.height - fm.height) / 2) + fm.ascent
		g.drawString(projName, x, y)
		g.dispose()
	}

}