package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.AppConfig
import com.dimdarkevil.noitaworld.model.Project
import java.io.File

val HOME = File(System.getProperty("user.home"))

const val DEFAULT_BIOME_COLOR = -65281

enum class DrawMode {
	DRAW,
	MOVE
}

fun getProjectFolder(config: AppConfig, p: Project) : File {
	val modFolder = File(File(config.noitaExeFile).parentFile, "mods")
	if (!modFolder.exists()) {
		throw RuntimeException("Mod folder ${modFolder.canonicalPath} does not exist")
	}
	return File(modFolder, p.name)
}