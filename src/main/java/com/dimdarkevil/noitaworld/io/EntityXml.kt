package com.dimdarkevil.noitaworld.io

import com.dimdarkevil.noitaworld.model.Rect
import com.dimdarkevil.noitaworld.model.Entity
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser
import java.io.File
import javax.imageio.ImageIO

object EntityXml {

	fun loadEntities(saveFolder: File, cb: ((String) -> Unit)? = null) : List<Entity> {
		val ents = File(saveFolder, "data/entities").walkTopDown()
			.filter { it.extension.lowercase() == "xml" }
			.map {
				cb?.let { callback -> callback("loading entity: ${it.name}") }
				loadEntity(saveFolder, it)
			}
		val entMap = ents.associateBy { it.entity_filename }
		ents.filter { it.image_filename.isEmpty() && it.base_filename.isNotEmpty() }.forEach { it.image_filename = entMap[it.base_filename]?.image_filename ?: "" }
		return ents.filter { it.image_filename.endsWith(".png")  }.map {
			val ifile = File(saveFolder, it.image_filename)
			if (ifile.exists()) {
				cb?.let { callback -> callback("loading image: ${ifile.name}") }
				if (it.sprite_rect.x >= 0) {
					val spritesheet = ImageIO.read(ifile)
					val w = if ((it.sprite_rect.x + it.sprite_rect.w) > spritesheet.width) {
						spritesheet.width - it.sprite_rect.x
					} else {
						it.sprite_rect.w
					}
					val h = if ((it.sprite_rect.y + it.sprite_rect.h) > spritesheet.height) {
						spritesheet.height - it.sprite_rect.y
					} else {
						it.sprite_rect.h
					}
					it.image = spritesheet.getSubimage(it.sprite_rect.x, it.sprite_rect.y, w, h)
				} else {
					it.image = ImageIO.read(ifile)
				}
			}
			if (it.sprite_rect.w == -1 && it.sprite_rect.h == -1) {
				it.image?.let { img ->
					it.sprite_rect = Rect(0, 0, img.width, img.height)
				}
			}
			it
		}.filter { it.image != null }.toList()
	}

	private fun loadEntity(saveFolder: File, xmlFile: File) : Entity {
		// PhysicsImageShapeComponent
		// SpriteComponent
		// UIInfoComponent
		val len = saveFolder.canonicalPath.length

		val doc = Jsoup.parse(xmlFile, "UTF-8", "", Parser.xmlParser())
		val base_file = doc.select("Base").firstOrNull()?.let { el ->
			el.attributes()["file"]
		} ?: ""
		val name = getNameFromEntityDoc(doc, saveFolder, base_file)
		val comp_sprite_file = doc.select("SpriteComponent").firstOrNull()?.let { el ->
			el.attributes()["image_file"]
		} ?: ""
		val physics_sprite_file = doc.select("PhysicsImageShapeComponent").firstOrNull()?.let { el ->
			el.attributes()["image_file"]
		} ?: ""
		var sprite_file = comp_sprite_file.ifEmpty { physics_sprite_file }

		val (rect, offsets) = if (sprite_file.isNotEmpty() && sprite_file.endsWith(".xml")) {
			val img_file = File(saveFolder, sprite_file)
			if (img_file.exists()) {
				val spriteInfo = loadSpriteInfo(img_file)
				if (spriteInfo.filename.isNotEmpty()) sprite_file = spriteInfo.filename
				Pair(Rect(spriteInfo.x, spriteInfo.y, spriteInfo.w, spriteInfo.h), Pair(spriteInfo.offset_x, spriteInfo.offset_y))
			} else {
				Pair(Rect(-1,-1,-1,-1), Pair(0, 0))
			}
		} else {
			Pair(Rect(-1,-1,-1,-1), Pair(0, 0))
		}

		val entity_filename = xmlFile.canonicalPath.substring(len+1).replace('\\', '/')
		val short_filename = entity_filename.split("/").drop(2).joinToString("/")
		return Entity(
			entity_filename = entity_filename,
			name = name,
			image_filename = sprite_file,
			base_filename = base_file,
			short_filename = short_filename,
			sprite_rect = rect,
			sprite_offset_x = offsets.first,
			sprite_offset_y = offsets.second,
		)
	}

	private fun getNameFromEntityDoc(doc: Document, saveFolder: File, base_file: String) : String {
		var name = doc.select("UIInfoComponent").firstOrNull()?.let { el ->
			el.attributes()["name"]
		} ?: ""
		if (name.isEmpty()) {
			name = doc.select("Entity").firstOrNull()?.let { el ->
				el.attributes()["name"]
			} ?: ""
		}
		if (name.isEmpty() && base_file.isNotEmpty()) {
			val bfile = File(saveFolder, base_file)
			if (bfile.exists()) {
				val bdoc = Jsoup.parse(bfile, "UTF-8", "", Parser.xmlParser())
				name = getNameFromEntityDoc(bdoc, saveFolder, "")
			}
		}
		return name
	}

	data class SpriteInfo(
		val filename: String,
		val x: Int,
		val y: Int,
		val w: Int,
		val h: Int,
		val offset_x: Int,
		val offset_y: Int,
	)

	private fun loadSpriteInfo(spriteXmlFile: File) : SpriteInfo {
		println(spriteXmlFile.canonicalPath)
		val sdoc = Jsoup.parse(spriteXmlFile, "UTF-8", "", Parser.xmlParser())
		val (filename, defAnim) = sdoc.select("Sprite").firstOrNull()?.let { el ->
			Pair(el.attributes()["filename"], el.attributes()["default_animation"])
		} ?: Pair("", "")
		val (xoffset, yoffset) = sdoc.select("Sprite").firstOrNull()?.let { el ->
			Pair(el.attributes()["offset_x"].ifBlank { "0" }.toFloat().toInt(), el.attributes()["offset_y"].ifBlank { "0" }.toFloat().toInt())
		} ?: Pair(0, 0)
		val rectAnim = sdoc.select("RectAnimation").firstOrNull { el ->
			el.attributes()["name"] == defAnim
		} ?: sdoc.select("RectAnimation").firstOrNull()

		return rectAnim?.let { el ->
			SpriteInfo(
				filename = filename,
				x = el.attributes()["pos_x"].toInt(),
				y = el.attributes()["pos_y"].toInt(),
				w = el.attributes()["frame_width"].toInt(),
				h = el.attributes()["frame_height"].toInt(),
				offset_x = xoffset,
				offset_y = yoffset
			)
		} ?: SpriteInfo(filename, -1, -1, -1, -1, 0, 0)
	}

}