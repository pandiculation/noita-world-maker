package com.dimdarkevil.noitaworld.io

import com.dimdarkevil.noitaworld.model.*
import com.dimdarkevil.swingutil.BackgroundWorker
import org.apache.commons.csv.CSVFormat
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileReader
import javax.imageio.ImageIO

class DataLoader(
	private val config: AppConfig,
	progressCallback: (Pair<String, Int>) -> Unit,
	doneCallback: (Boolean, String, NoitaData?) -> Unit,
	abortCallback: () -> Boolean
) : BackgroundWorker<NoitaData>(progressCallback, doneCallback, abortCallback)
{
	override fun exec(shouldAbort: () -> Boolean): NoitaData {
		if (!File(config.noitaExeFile).exists()) {
			throw RuntimeException("Noita exe file not found")
		}
		publish(Pair("loading translations|", 0))
		val translations = loadTranslations(config) {
			publish(Pair("loading translations|$it", 0))
		}
		publish(Pair("loading biomes|", 20))
		val biomes = loadBiomesAll(config, translations) {
			publish(Pair("loading biomes|$it", 20))
		}
		publish(Pair("loading materials|", 40))
		val materials = loadMaterials(config, translations) {
			publish(Pair("loading materials|$it", 40))
		}
		publish(Pair("loading spells|", 60))
		val spells = loadSpells(config, translations) {
			publish(Pair("loading spells|$it", 60))
		}
		publish(Pair("loading perks|", 60))
		val perks = loadPerks(config, translations) {
			publish(Pair("loading perks|$it", 60))
		}
		publish(Pair("loading entities|", 80))
		val entities = loadEntities(config, translations) {
			publish(Pair("loading entities|$it", 80))
		}
		val specials = loadSpecials(config)
		publish(Pair("done|", 100))
		return NoitaData(
			translations = translations,
			biomes = biomes,
			materials = materials,
			entities = entities,
			spells = spells,
			perks = perks,
			specials = specials,
		)
	}
}

data class NoitaData(
	val translations: Map<String, String>,
	val biomes: List<Biome>,
	val materials: List<Material>,
	val entities: List<Entity>,
	val spells: List<Spell>,
	val perks: List<Perk>,
	val specials: List<Special>,
)

/*
fun loadNoitaData(config: AppConfig, progressFn: (msg: String, pct: Int) -> Unit) : NoitaData {
	progressFn("loading translations", 0)
	val translations = loadTranslations(config)
	progressFn("loading biomes", 25)
	val biomes = loadBiomesAll(config, translations)
	progressFn("loading materials", 50)
	val materials = loadMaterials(config, translations)
	progressFn("loading entities", 75)
	val entities = loadEntities(config, translations)
	progressFn("done", 100)
	return NoitaData(
		translations = translations,
		biomes = biomes,
		materials = materials,
		entities = entities
	)
}
*/

fun loadTranslations(config: AppConfig, cb: ((String) -> Unit)? = null) : Map<String,String> {
	val transFile = getTranslationsFile(config)
	val trans = FileReader(transFile).use { rdr ->
		CSVFormat.DEFAULT.parse(rdr).records.associate { rec ->
			"\$${rec[0].trim()}" to rec[1].trim()
		}
	}
	return trans
}

fun loadBiomesAll(config: AppConfig, translations: Map<String,String>, cb: ((String) -> Unit)? = null) : List<Biome> {
	val biomesAllFile = getBiomesAllFile(config)
	val biomes = BiomesAllXml.loadBiomesAll(biomesAllFile).sortedBy { it.biome_filename }
	biomes.forEach { biome ->
		cb?.let { it("biome: ${biome.biome_filename}") }
		BiomeXml.loadBiome(biome, File(config.noitaSaveFolder, biome.biome_filename))
		biome.english_name = translations[biome.name] ?: biome.name
		biome.short_filename = biome.biome_filename.split("/").let {
			if (it.size == 3) {
				it.last()
			} else {
				it.drop(it.size-2).joinToString("/")
			}
		}
	}
	return biomes
}

fun loadMaterials(config: AppConfig, translations: Map<String,String>, cb: ((String) -> Unit)? = null) : List<Material> {
	val matFile = File(File(config.noitaSaveFolder), "data/materials.xml")
	val materials = MaterialXml.loadMaterials(matFile).groupBy {
		it.wang_color.uppercase()
	}.map { (color, mats) ->
		mats.first()
	}
	materials.forEach { mat ->
		cb?.let { it("material: ${mat.ui_name}") }
		if (mat.ui_name.isNotEmpty()) mat.english_name = translations[mat.ui_name] ?: mat.name
	}
	//val matGroups = materials.groupBy { it.wang_color.uppercase().substring(2) }.filter { it.value.size > 1 }
	val colors = materials.map { it.wang_color.uppercase().substring(2) }.toSet()
	val grayCnt = colors.count {
		val r = it.substring((0..1))
		val g = it.substring((2..3))
		val b = it.substring((4..5))
		(r == g && g == b)
	}
	println("there are ${colors.size} colors and ${materials.size} materials")
	println("gray count: $grayCnt")
	return materials
}

fun loadEntities(config: AppConfig, translations: Map<String, String>, cb: ((String) -> Unit)? = null) : List<Entity> {
	val entities = EntityXml.loadEntities(File(config.noitaSaveFolder), cb)
	entities.forEach { ent ->
		if (ent.name.isNotEmpty()) ent.english_name = translations[ent.name] ?: ent.name
	}
	return entities
}

private val specialList = listOf(
	Special("specific_potion", "potion with specific contents","data/items_gfx/potion.png", PotionConfig::class),
	Special("specific_chest", "chest with specific contents", "data/buildings_gfx/chest_random.png", ChestConfig::class),
	Special("teleport_to_loc", "teleport to location", "data/buildings_gfx/teleport_center.png", TeleportConfig::class, 48, 48),
)

fun loadSpecials(config: AppConfig) : List<Special> {
	val saveFolder = File(config.noitaSaveFolder)
	specialList.forEach { s ->
		s.image = ImageIO.read(File(saveFolder, s.image_filename))
	}
	return specialList
}

fun loadSpells(config: AppConfig, translations: Map<String, String>, cb: ((String) -> Unit)? = null) : List<Spell> {
	val f = getGunActionsFile(config)
	val lines = f.readLines(Charsets.UTF_8).filterLuaComments()
	val spells = mutableListOf<Spell>()
	var curSpell = Spell()
	lines.forEach { line ->
		val l = line.trim()
		if (l == "},") {
			cb?.let { it("spell: ${curSpell.id}") }
			curSpell.english_name = translations[curSpell.name] ?: curSpell.name
			curSpell.english_desc = translations[curSpell.description] ?: curSpell.description
			spells.add(curSpell)
			curSpell = Spell()
		} else {
			when {
				l.startsWith("id") -> curSpell.id = l.getRightOfEqual()
				l.startsWith("name") -> curSpell.name = l.getRightOfEqual()
				l.startsWith("description") -> curSpell.description = l.getRightOfEqual()
				(l.startsWith("sprite") && !l.startsWith("sprite_")) -> curSpell.sprite = l.getRightOfEqual()
			}
		}
	}
	val saveFolder = File(config.noitaSaveFolder)
	spells.forEach { spell ->
		if (spell.sprite.isNotEmpty()) {
			val bi = ImageIO.read(File(saveFolder, spell.sprite))
			val bb = BufferedImage(bi.width, bi.height, BufferedImage.TYPE_INT_ARGB)
			val g = bb.createGraphics()
			g.color = Color.BLACK
			g.fillRect(0, 0, bb.width, bb.height)
			g.drawImage(bi, 0, 0, null)
			g.dispose()
			spell.image = bb
		}
	}
	return spells.sortedBy { it.english_name }
}

fun loadPerks(config: AppConfig, translations: Map<String, String>, cb: ((String) -> Unit)? = null) : List<Perk> {
	val f = getPerkListFile(config)
	val lines = f.readLines(Charsets.UTF_8).filterLuaComments()
	val perks = mutableListOf<Perk>()
	var curPerk = Perk()
	lines.forEach { line ->
		val l = line.trim()
		if (l == "},") {
			cb?.let { it("perk: ${curPerk.id}") }
			curPerk.english_name = translations[curPerk.ui_name] ?: curPerk.ui_name
			curPerk.english_desc = translations[curPerk.ui_description] ?: curPerk.ui_description
			perks.add(curPerk)
			curPerk = Perk()
		} else {
			when {
				l.startsWith("id") -> curPerk.id = l.getRightOfEqual()
				l.startsWith("ui_name") -> curPerk.ui_name = l.getRightOfEqual()
				l.startsWith("ui_description") -> curPerk.ui_description = l.getRightOfEqual()
				l.startsWith("ui_icon") -> curPerk.ui_icon = l.getRightOfEqual()
				l.startsWith("perk_icon") -> curPerk.perk_icon = l.getRightOfEqual()
			}
		}
	}
	val saveFolder = File(config.noitaSaveFolder)
	perks.forEach { perk ->
		if (perk.ui_icon.isNotEmpty()) {
			perk.image = ImageIO.read(File(saveFolder, perk.perk_icon))
		}
	}
	return perks.sortedBy { it.english_name }
}

fun String.getRightOfEqual() : String {
	return this.split("=").last().trim().unquote()
}

fun String.unquote() : String {
	return if (this.startsWith("\"") && this.endsWith("\"")) {
		this.substring((1..this.length-2))
	} else if (this.startsWith("\"") && this.endsWith("\",")) {
		this.substring((1..this.length-3))
	} else {
		this
	}
}

fun List<String>.filterLuaComments() : List<String> {
	val newLines = mutableListOf<String>()
	var inComment = false
	this.forEach { line ->
		val l = line.trim()
		when {
			inComment -> {
				if (l.endsWith("]]--")) {
					inComment = false
				}
			}
			else -> {
				if (l.startsWith("--[[")) {
					inComment = true
				} else {
					newLines.add(line)
				}
			}
		}
	}
	return newLines
}

fun getGunActionsFile(config: AppConfig) : File {
	val gunActionsFile = File(File(config.noitaSaveFolder), "data/scripts/gun/gun_actions.lua")
	if (!gunActionsFile.exists()) {
		throw RuntimeException("spells file does not exist: ${gunActionsFile.canonicalPath}")
	}
	return gunActionsFile
}

fun getPerkListFile(config: AppConfig) : File {
	val perkListFile = File(File(config.noitaSaveFolder), "data/scripts/perks/perk_list.lua")
	if (!perkListFile.exists()) {
		throw RuntimeException("perks file does not exist: ${perkListFile.canonicalPath}")
	}
	return perkListFile
}

fun getBiomesAllFile(config: AppConfig) : File {
	val biomesAllFile = File(File(config.noitaSaveFolder), "data/biome/_biomes_all.xml")
	if (!biomesAllFile.exists()) {
		throw RuntimeException("biomes file does not exist: ${biomesAllFile.canonicalPath}")
	}
	return biomesAllFile
}

fun getTranslationsFile(config: AppConfig) : File {
	val transFile = File(File(config.noitaExeFile).parentFile, "data/translations/common.csv")
	if (!transFile.exists()) {
		throw RuntimeException("translations file does not exist: ${transFile.canonicalPath}")
	}
	return transFile
}

