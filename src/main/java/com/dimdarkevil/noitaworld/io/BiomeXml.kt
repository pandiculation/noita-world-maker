package com.dimdarkevil.noitaworld.io

import com.dimdarkevil.noitaworld.HOME
import com.dimdarkevil.noitaworld.model.Biome
import org.jsoup.Jsoup
import org.jsoup.parser.Parser
import java.io.File

object BiomeXml {
	val saveFolder = File(HOME, "AppData/LocalLow/Nolla_Games_Noita")

	@JvmStatic
	fun main(args: Array<String>) {
		val file = File(saveFolder, "data/biome/_biomes_all.xml")
		val biomes = BiomesAllXml.loadBiomesAll(file)
		biomes.forEach {
			loadBiome(it, File(saveFolder, it.biome_filename))
		}
		biomes.forEach { println("${it}") }
	}

	fun loadBiome(biome: Biome, xmlFile: File) {
		val doc = Jsoup.parse(xmlFile, "UTF-8", "", Parser.xmlParser())
		doc.select("Topology").map { el ->
			biome.name = el.attributes()["name"]
			biome.type = el.attributes()["type"]
			biome.lua_script = el.attributes()["lua_script"]
			biome.wang_template_file = el.attributes()["wang_template_file"]
		}.toSet()
	}

}