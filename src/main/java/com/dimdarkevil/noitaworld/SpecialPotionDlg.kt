package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.cellrenderer.BiomeTableColorCellRenderer
import com.dimdarkevil.noitaworld.model.AppConfig
import com.dimdarkevil.noitaworld.model.Material
import com.dimdarkevil.noitaworld.model.PotionConfig
import com.dimdarkevil.noitaworld.tablemodel.MaterialTableModel
import com.dimdarkevil.swingutil.OkCancelModalDialog
import com.dimdarkevil.swingutil.PaddedPanel
import java.awt.BorderLayout
import java.awt.event.ActionEvent
import javax.swing.*

class SpecialPotionDlg(owner: JFrame, title: String, val config: AppConfig, materials: List<Material>) : OkCancelModalDialog<PotionConfig>(owner, title) {
	private val liquids = materials.filter { it.tags.contains("[liquid]") && !it.tags.contains("[solid]") && !it.tags.contains("[evaporable]") && !it.tags.contains("[evaporable_fast]") }
	private val matPanel = JPanel(BorderLayout())
	private val matSearchTextField = JTextField()
	private val matTableModel = MaterialTableModel()
	private val matTable = JTable(matTableModel)
	private val matScrollPane = JScrollPane(matTable)

	init {
		matTable.setDefaultRenderer(Int::class.java, BiomeTableColorCellRenderer())
		matTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION
		matTableModel.setMaterials(liquids)
	}

	override fun buildUi(): JPanel {
		val matSearchPanel = PaddedPanel()
		matSearchPanel.layout = BoxLayout(matSearchPanel, BoxLayout.X_AXIS)
		matSearchPanel.add(JLabel("search:"))
		matSearchPanel.add(matSearchTextField)
		matPanel.add(matSearchPanel, BorderLayout.NORTH)
		matPanel.add(matScrollPane, BorderLayout.CENTER)

		matSearchTextField.addActionListener { onMatSearchText(it) }

		return matPanel
	}

	fun withMat(name: String) : SpecialPotionDlg {
		liquids.indexOfFirst { it.name == name }.let { idx ->
			if (idx >= 0) {
				matTable.setRowSelectionInterval(idx, idx)
			}
		}
		return this
	}

	override fun getResult(): PotionConfig {
		matTableModel.getMaterialAt(matTable.selectedRow)?.let {
			return PotionConfig(it.name)
		} ?: throw RuntimeException("select a liquid")
	}

	private fun onMatSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			matTableModel.setMaterials(liquids)
		} else {
			matTableModel.setMaterials(liquids.filter {
				it.ui_name.contains(txt, true) ||
					it.name.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

}