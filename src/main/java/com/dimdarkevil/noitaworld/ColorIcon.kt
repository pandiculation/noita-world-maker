package com.dimdarkevil.noitaworld

import java.awt.Color
import java.awt.Component
import java.awt.Graphics
import javax.swing.Icon

class ColorIcon(rgb: Int, val width: Int, val height: Int) : Icon {
	val color = Color(rgb)

	override fun paintIcon(c: Component, g: Graphics, x: Int, y: Int) {
		g.color = color
		g.fillRect(x, y, iconWidth, iconHeight)
	}

	override fun getIconWidth() = width

	override fun getIconHeight() = height
}