package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.Project
import com.dimdarkevil.swingutil.EmUnit
import com.dimdarkevil.swingutil.OkCancelModalDialog
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*
import javax.swing.border.EmptyBorder

class CoordinateDlg(owner: JFrame, title: String, val curX: Int, val curY: Int, val project: Project) : OkCancelModalDialog<Pair<Int,Int>>(owner, title) {
	private val xSpinner = JSpinner()
	private val ySpinner = JSpinner()

	override fun buildUi(): JPanel {
		val coordPanel = JPanel()
		coordPanel.layout = BoxLayout(coordPanel, BoxLayout.Y_AXIS)
		preferredSize = Dimension(EmUnit.EM_SIZE * 30, xSpinner.preferredSize.height * 8)
		xSpinner.model = SpinnerNumberModel(curX, ((project.biome_map.width / 2) * 512) * -1,  (project.biome_map.width / 2) * 512, 10)
		ySpinner.model = SpinnerNumberModel(curY, (project.biome_map.startY * 512) * -1,  (project.biome_map.height - project.biome_map.startY) * 512, 10)

		val xPanel = JPanel()
		xPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		xPanel.border = EmptyBorder(0, 5, 0, 5)
		xPanel.add(JLabel("X:"))
		xPanel.add(xSpinner)

		val yPanel = JPanel()
		yPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		yPanel.border = EmptyBorder(0, 5, 0, 5)
		yPanel.add(JLabel("Y:"))
		yPanel.add(ySpinner)

		coordPanel.add(xPanel)
		coordPanel.add(yPanel)

		return coordPanel
	}

	override fun getResult(): Pair<Int, Int> {
		return Pair(xSpinner.value as Int, ySpinner.value as Int)
	}
}