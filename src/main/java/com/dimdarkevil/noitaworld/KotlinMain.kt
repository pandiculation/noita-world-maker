package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.AppConfig
import com.dimdarkevil.swingutil.loadConfig
import com.dimdarkevil.swingutil.saveConfig
import java.awt.EventQueue
import java.awt.Toolkit
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.lang.Exception
import javax.imageio.ImageIO
import javax.swing.JFrame
import javax.swing.UIManager
import javax.swing.UnsupportedLookAndFeelException

const val APP_NAME = "noita-world-maker"

object KotlinMain {
	@JvmStatic
	fun main(args: Array<String>) {
		val config : AppConfig = loadConfig(APP_NAME, AppConfig())
		try {
			System.setProperty("apple.awt.application.name", APP_NAME)
			UIManager.setLookAndFeel(
				UIManager.getSystemLookAndFeelClassName())
		} catch (e: UnsupportedLookAndFeelException) {
			// handle exception
		} catch (e: ClassNotFoundException) {
			// handle exception
		} catch (e: InstantiationException) {
			// handle exception
		} catch (e: IllegalAccessException) {
			// handle exception
		}
		val screenSize = Toolkit.getDefaultToolkit().screenSize

		if (config.winWidth < 0) config.winWidth = 1600
		if (config.winHeight < 0) config.winHeight = 1000
		if (config.winX < 0) config.winX = (screenSize.width - config.winWidth) / 2
		if (config.winY < 0) config.winY = (screenSize.height - config.winHeight) / 2

		val icons = listOf(
			ImageIO.read(javaClass.getResourceAsStream("/noita-world-16.png")),
			ImageIO.read(javaClass.getResourceAsStream("/noita-world-32.png")),
			ImageIO.read(javaClass.getResourceAsStream("/noita-world-64.png")),
			ImageIO.read(javaClass.getResourceAsStream("/noita-world-128.png")),
			ImageIO.read(javaClass.getResourceAsStream("/noita-world-256.png")),
		)

		UIManager.put("FileChooser.readOnly", true)
		val pframe = JFrame(APP_NAME)
		pframe.setSize(480, 180)
		pframe.setLocationRelativeTo(null)
		pframe.setLocation((screenSize.width - 480)/2, (screenSize.height - 180) / 2)
		val loadingScreen = LoadingScreen(pframe, config)
		pframe.contentPane = loadingScreen.mainPanel
		pframe.defaultCloseOperation = JFrame.DO_NOTHING_ON_CLOSE
		EventQueue.invokeLater {
			pframe.isVisible = true
		}
		while (!loadingScreen.done) {
			Thread.sleep(500)
		}
		val success = loadingScreen.succeeded
		val noitaData = loadingScreen.noitaData
		pframe.dispose()

		if (!success) {
			val sframe = JFrame(APP_NAME)
			sframe.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
			val (ssuccess, folders) = SettingsDlg(config, sframe, "noita-world-builder-settings").showModal()
			if (ssuccess && folders != null) {
				config.noitaSaveFolder = folders.first
				config.noitaExeFile = folders.second
				saveConfig(APP_NAME, config)
			}
			sframe.dispose()
		} else {
			val frame = JFrame(APP_NAME)
			val mainForm = MainForm(config, frame, noitaData!!)
			frame.contentPane = mainForm.mainPanel

			frame.setSize(config.winWidth, config.winHeight)
			frame.setLocationRelativeTo(null)
			frame.setLocation(config.winX, config.winY)
			frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
			frame.iconImages = icons
			//frame.iconImage = ImageIcon(javaClass.getResource("/pipewrench.png")).image
			frame.addWindowListener(object: WindowAdapter() {
				override fun windowClosing(e: WindowEvent) {
					mainForm.updateConfig()
					saveConfig(APP_NAME, config)
				}
			})
			EventQueue.invokeLater {frame.isVisible = true}
		}
	}

}