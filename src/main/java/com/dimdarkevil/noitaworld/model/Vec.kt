package com.dimdarkevil.noitaworld.model

data class Vec2(var x: Double, var y: Double) {
	operator fun unaryMinus() = Vec2(-x, -y)
	operator fun plus(v: Vec2) = Vec2(this.x + v.x, this.y + v.y)
	operator fun minus(v: Vec2) = Vec2(this.x - v.x, this.y - v.y)
	operator fun times(v: Vec2) = Vec2(this.x * v.x, this.y * v.y)
	operator fun times(sc: Float) = Vec2(x * sc, y * sc)
}

data class Vec2i(var x: Int, var y: Int)
