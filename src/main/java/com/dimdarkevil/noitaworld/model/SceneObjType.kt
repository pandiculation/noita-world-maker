package com.dimdarkevil.noitaworld.model

enum class SceneObjType {
	ENTITY,
	PERK,
	SPELL,
	SPECIAL,
}