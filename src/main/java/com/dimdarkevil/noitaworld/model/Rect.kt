package com.dimdarkevil.noitaworld.model

data class Rect(
	val x: Int,
	val y: Int,
	val w: Int,
	val h: Int,
)
