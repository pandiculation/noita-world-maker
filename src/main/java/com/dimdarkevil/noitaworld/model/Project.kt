package com.dimdarkevil.noitaworld.model

data class Project(
	var name: String = "",
	var description: String = "",
	var pretty_name: String = "",
	var pretty_description: String = "",
	var playerStartX: Int = 0,
	var playerStartY: Int = 0,
	val biome_map: BiomeMap = BiomeMap(),
	val pixel_scenes: MutableList<Scene> = mutableListOf()
)
