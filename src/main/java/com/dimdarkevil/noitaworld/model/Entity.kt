package com.dimdarkevil.noitaworld.model

import java.awt.image.BufferedImage

data class Entity(
	val entity_filename: String,
	val name: String,
	var image_filename: String,
	val base_filename: String,
	var short_filename: String = "",
	var image: BufferedImage? = null,
	var english_name: String = "",
	var sprite_rect: Rect,
	var sprite_offset_x: Int = 0,
	var sprite_offset_y: Int = 0,
)
