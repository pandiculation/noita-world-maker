package com.dimdarkevil.noitaworld.model

data class Scene(
	val name: String = "",
	var x: Int = 0,
	var y: Int = 0,
	var w: Int = 0,
	var h: Int = 0,
	val objects: MutableList<SceneObj> = mutableListOf(),
)


