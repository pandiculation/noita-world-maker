package com.dimdarkevil.noitaworld.model

data class Biome(
	val biome_filename: String,
	val height_index: Int,
	val color: String,
	val rgb: Int = color.toLong(16).toInt(),
	var name: String = "",
	var type: String = "",
	var lua_script: String = "",
	var wang_template_file: String = "",
	var english_name: String = "",
	var short_filename: String = "",
)
