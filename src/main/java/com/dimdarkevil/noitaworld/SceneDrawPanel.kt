package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.listener.SceneStatusListener
import com.dimdarkevil.noitaworld.model.*
import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.event.*
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import javax.swing.JPanel

class SceneDrawPanel(val statusListener: SceneStatusListener) : JPanel(), ComponentListener, MouseListener, MouseMotionListener, MouseWheelListener {
	private var matImage : BufferedImage? = null
	private var visImage : BufferedImage? = null
	private var bkgImage : BufferedImage? = null
	private var pixelWidth = 0
	private var pixelHeight = 0
	private val viewport = Viewport(pixelWidth, pixelHeight, 1.0)
	private var lastMouseX = -1
	private var lastMouseY = -1
	private var dragging = false
	private var draggingObject: SceneObj? = null
	private var dragObjOffX = 0
	private var dragObjOffY = 0
	private var panning = false
	private var drawMode = DrawMode.DRAW
	var paintColor = DEFAULT_BIOME_COLOR
	private var biWidth = 0 // prevents having to reference the image all the time
	private var biHeight = 0 // prevents having to reference the image all the time
	private val atf = AffineTransform()
	private var objects: List<SceneObj> = listOf()

	private val undos = mutableListOf<Undo>()
	private val alpha = 0xFF000000
	private var initialized = false
	private val lastMovePos = Vec2i(0, 0)
	var matMap = mapOf<Int, Material>()
	var entMap = mapOf<String,Entity>()
	var spellMap = mapOf<String, Spell>()
	var perkMap = mapOf<String,Perk>()
	var specialMap = mapOf<String,Special>()
	private val holdPos = Vec2i(0, 0)
	//private val emptyMat = Material(wang_color = "ff")

	init {
		addMouseWheelListener(this)
		addMouseListener(this)
		addMouseMotionListener(this)
		//popupMenu.add(newVarMenuItem)
		//newVarMenuItem.addActionListener { addVariable() }
		isOpaque = true
		addComponentListener(this)
	}

	override fun paintComponent(gg: Graphics?) {
		//println("paintComponent")
		super.paintComponent(gg)
		if (!initialized) return
		val mbi = matImage ?: return
		val vbi = visImage ?: return
		val g = gg as Graphics2D

		val scale = viewport.getScale()
		val tl = viewport.getTopLeft()
		val br = viewport.getBottomRight()
		val sx = maxOf(0, tl.x.toInt())
		val sy = maxOf(0, tl.y.toInt())
		val ex = minOf(br.x.toInt(), mbi.width)
		val ey = minOf(br.y.toInt(), mbi.height)
		//println("paintComponent sx=$sx, sy=$sy, ex=$ex, ey=$ey")

		(sy until ey).forEach { y ->
			(sx until ex).forEach { x ->
				val ci = mbi.getRGB(x, y)
				if ((ci.toLong() and alpha) > 0L) {
					val tf = g.transform
					// reset our affine transform (rather than creating garbage on each draw)
					atf.setToIdentity()
					atf.scale(scale, scale)
					atf.translate(-tl.x, -tl.y)
					g.transform(atf)
					//g.stroke = stroke
					g.color = Color(ci)
					g.fillRect(x, y, 1, 1)
					g.transform = tf
				}
			}
		}

		objects.forEach { o ->
			when (o.type) {
				SceneObjType.ENTITY -> entMap[o.resource_name]?.image
				SceneObjType.PERK -> perkMap[o.resource_name]?.image
				SceneObjType.SPELL -> spellMap[o.resource_name]?.image
				SceneObjType.SPECIAL -> specialMap[o.resource_name]?.image
			}?.let { img ->
				if (viewport.inBounds(o.x.toDouble(), o.y.toDouble(), o.w.toDouble(), o.h.toDouble())) {
					val tf = g.transform
					// reset our affine transform (rather than creating garbage on each draw)
					atf.setToIdentity()
					atf.scale(scale, scale)
					atf.translate(-tl.x, -tl.y)
					g.transform(atf)
					g.drawImage(img, o.x, o.y, null)
					g.color = Color.BLACK
					g.drawRect(o.x, o.y, o.w, o.h)
					g.transform = tf
				}
			}
		}
	}

	override fun componentResized(e: ComponentEvent) {
		pixelWidth = e.component.width
		pixelHeight = e.component.height
		println("sceneDrawPanel componentResized $pixelWidth x $pixelHeight")
		viewport.resize(pixelWidth, pixelHeight)
		initialized = true
		repaint()
	}
	override fun componentHidden(e: ComponentEvent) { }
	override fun componentMoved(e: ComponentEvent) { }
	override fun componentShown(e: ComponentEvent) {
		println("sceneDrawPanel componentShown ${e.component.width} ${e.component.height}")
	}

	override fun mouseClicked(e: MouseEvent) {
		when (e.button) {
			MouseEvent.BUTTON1 -> { }
			MouseEvent.BUTTON2 -> { }
			MouseEvent.BUTTON3 -> { }
		}
	}

	override fun mouseWheelMoved(e: MouseWheelEvent) {
		val changeAmount = -((e.wheelRotation.toDouble() * 0.04))
		var newScale = viewport.getScale() * ( 1.0 + changeAmount)
		if (newScale > 100.0) newScale = 100.0
		if (newScale <= 1.0) newScale = 1.0
		viewport.rescaleAroundPoint(e.x, e.y, newScale)
		repaint()
	}

	override fun mousePressed(e: MouseEvent) {
		drawMode = if (e.isShiftDown) DrawMode.MOVE else DrawMode.DRAW
		if (e.button == MouseEvent.BUTTON2) {
			panning = true
			lastMouseX = e.x
			lastMouseY = e.y
		} else if (e.button == MouseEvent.BUTTON1) {
			dragging = true
			when (drawMode) {
				DrawMode.DRAW -> {
					undos.add(Undo())
					drawPixelFromMouseEvent(e)
				}
				DrawMode.MOVE -> {
					viewport.getWorldPosInt(e.x, e.y).let { wp ->
						objects.firstOrNull{ o -> isMouseOverObject(wp.x, wp.y, o) }?.let { o ->
							draggingObject = o
							holdPos.x = o.x
							holdPos.y = o.y
							dragObjOffX = o.x - wp.x
							dragObjOffY = o.y - wp.y
						}
					}
				}
			}
		} else if (e.button == MouseEvent.BUTTON3) {
			matImage?.let { bi ->
				viewport.getWorldPosInt(e.x, e.y).let { wp ->
					val ix = wp.x.toInt()
					val iy = wp.y.toInt()
					if (ix >= 0 && ix < biWidth && iy >= 0 && iy < biHeight) {
						val ci = bi.getRGB(ix, iy)
						statusListener.replacementColorSelected(ci)
					}
				}
			}
		}
	}

	override fun mouseDragged(e: MouseEvent) {
		if (!panning && !dragging) return
		if (panning) {
			viewport.moveByPix(lastMouseX - e.x, lastMouseY - e.y)
			repaint()
			lastMouseX = e.x
			lastMouseY = e.y
		} else if (dragging) {
			when (drawMode) {
				DrawMode.DRAW -> drawPixelFromMouseEvent(e)
				DrawMode.MOVE -> {
					draggingObject?.let { o ->
						viewport.getWorldPosInt(e.x, e.y).let { wp ->
							o.x = wp.x + dragObjOffX
							o.y = wp.y + dragObjOffY
							// TODO: calculate correct repaint rect
							repaint()
						}
					}
				}
			}
		}
	}

	override fun mouseMoved(e: MouseEvent) {
		val bi = matImage ?: return
		viewport.getWorldPosInt(e.x, e.y).let { wp ->
			val ix = wp.x.toInt()
			val iy = wp.y.toInt()
			if (lastMovePos.x == ix && lastMovePos.y == iy) return
			lastMovePos.x = ix
			lastMovePos.y = iy
			if (ix >= 0 && ix < biWidth && iy >= 0 && iy < biHeight) {
				val ci = bi.getRGB(ix, iy)
				val matMsg = matMap[ci]?.let {
					"${it.ui_name} (${it.wang_color})"
				} ?: ""
				statusListener.showStatus(ix, iy, matMsg)
			}
		}
	}

	override fun mouseReleased(e: MouseEvent) {
		if (draggingObject != null) {
			draggingObject?.let {
				statusListener.objectPosChanged(it)
				undos.add(Undo(mutableListOf(ObjectChange(it, holdPos.x, holdPos.y, it.x, it.y))))
			}
		}
		if (undos.isNotEmpty() && undos[undos.size-1].changes.isEmpty()) {
			undos.removeLast()
		}
		viewport.report()
		panning = false
		dragging = false
		draggingObject = null
	}

	override fun mouseEntered(e: MouseEvent) {
	}

	override fun mouseExited(e: MouseEvent) {
	}

	private fun drawPixelFromMouseEvent(e: MouseEvent) {
		matImage?.let { bi ->
			viewport.getWorldPos(e.x, e.y).let { wp ->
				val x = wp.x.toInt()
				val y = wp.y .toInt()
				if (x >= 0 && x < bi.width && y >= 0 && y < bi.height) {
					val curRgb = bi.getRGB(x, y)
					bi.setRGB(x, y, paintColor)
					if (curRgb != paintColor) undos[undos.size-1].changes.add(PixelChange(x, y, curRgb, paintColor))
					val pp = viewport.getPixelPos(x.toDouble(), y.toDouble())
					val scale = (viewport.getScale() + 1.0).toInt()
					repaint(pp.x, pp.y, scale, scale)
				}
			}
		}
	}

	fun replaceColor(srcClr: Int, dstColor: Int) {
		val bi = matImage ?: return
		val undo = Undo()
		(0 until bi.height).forEach { y ->
			(0 until bi.width).forEach { x ->
				val ci = bi.getRGB(x, y)
				if (ci == srcClr) {
					bi.setRGB(x, y, dstColor)
					undo.changes.add(PixelChange(x, y, srcClr, dstColor))
				}
			}
		}
		undos.add(undo)
		repaint()
	}

	fun setImages(mbi: BufferedImage, vbi: BufferedImage, bbi: BufferedImage) {
		println("setImages")
		matImage = mbi
		visImage = vbi
		bkgImage = bbi
		biWidth = mbi.width
		biHeight = mbi.height
		viewport.resize(this.width, this.height)
		repaint()
	}

	fun fitImage() {
		matImage?.let { bi ->
			println("fitImage - $biWidth, $biHeight")
			viewport.fitImage(biWidth, biHeight)
		}
	}

	fun getMatImg() = matImage
	fun getVisImg() = visImage
	fun getBkgImg() = bkgImage

	fun setObjects(olist: List<SceneObj>) {
		objects = olist
		repaint()
	}

	fun undo() {
		val bi = matImage ?: return
		if (undos.isEmpty()) return
		val undo = undos.removeLast()
		undo.changes.forEach { ch ->
			when (ch) {
				is PixelChange -> bi.setRGB(ch.x, ch.y, ch.fromColor)
				is PlayerStartChange -> {}
				is SceneChange -> {}
				is ObjectChange -> {
					ch.obj.x = ch.fromX
					ch.obj.y = ch.fromY
					statusListener.objectPosChanged(ch.obj)
				}
			}
		}
		repaint()
	}

	private fun isMouseOverObject(mx: Int, my: Int, o: SceneObj) : Boolean {
		return (mx >= o.x && mx < (o.x+o.w) && my >= o.y && my < (o.y + o.h))
	}


}