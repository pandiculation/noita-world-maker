package com.dimdarkevil.noitaworld.tablemodel

import com.dimdarkevil.noitaworld.model.SceneObj
import javax.swing.table.AbstractTableModel

class ObjectTableModel : AbstractTableModel() {
	private var items = listOf<SceneObj>()

	override fun getRowCount() : Int {
		return items.size
	}

	override fun getColumnCount() : Int {
		return 5
	}

	override fun getValueAt(rowIndex: Int, columnIndex: Int): Any? {
		if (rowIndex < 0 || rowIndex >= items.size) return null
		val b = items[rowIndex]
		return when (columnIndex) {
			0 -> b.id
			1 -> b.short_name
			2 -> b.type.name
			3 -> b.x
			4 -> b.y
			else -> "[unknown]"
		}
	}

	override fun getColumnName(column: Int): String {
		return when (column) {
			0 -> "id"
			1 -> "filename"
			2 -> "type"
			3 -> "x"
			4 -> "y"
			else -> "[unknown]"
		}
	}

	fun setObjects(lst: List<SceneObj>) {
		items = lst
		this.fireTableDataChanged()
	}

	fun getObjectAt(row: Int) : SceneObj? {
		if (row < 0 || row >= items.size) return null
		return items[row]
	}
}