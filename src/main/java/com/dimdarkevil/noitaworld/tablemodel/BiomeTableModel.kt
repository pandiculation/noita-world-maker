package com.dimdarkevil.noitaworld.tablemodel

import com.dimdarkevil.noitaworld.model.Biome
import javax.swing.table.AbstractTableModel

class BiomeTableModel : AbstractTableModel() {
	private var items = listOf<Biome>()

	override fun getRowCount() : Int {
		return items.size
	}

	override fun getColumnCount() : Int {
		return 3
	}

	override fun getValueAt(rowIndex: Int, columnIndex: Int): Any? {
		val b = items[rowIndex]
		return when (columnIndex) {
			0 -> b.rgb
			1 -> b.short_filename
			2 -> b.english_name
			else -> "[unknown]"
		}
	}

	override fun getColumnName(column: Int): String {
		return when (column) {
			0 -> "color"
			1 -> "file"
			2 -> "name"
			else -> "[unknown]"
		}
	}

	override fun getColumnClass(columnIndex: Int): Class<*> {
		return when (columnIndex) {
			0 -> Int::class.java
			else -> String::class.java
		}
	}

	fun setBiomes(lst: List<Biome>) {
		items = lst
		this.fireTableDataChanged()
	}

	fun getBiomeAt(row: Int) : Biome? {
		if (row < 0 || row >= items.size) return null
		return items[row]
	}

}