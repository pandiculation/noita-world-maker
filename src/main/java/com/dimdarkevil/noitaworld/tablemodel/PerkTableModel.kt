package com.dimdarkevil.noitaworld.tablemodel

import com.dimdarkevil.noitaworld.model.Perk
import java.awt.image.BufferedImage
import javax.swing.table.AbstractTableModel

class PerkTableModel : AbstractTableModel() {
	private var items = listOf<Perk>()

	override fun getRowCount() : Int {
		return items.size
	}

	override fun getColumnCount() : Int {
		return 3
	}

	override fun getValueAt(rowIndex: Int, columnIndex: Int): Any? {
		val b = items[rowIndex]
		return when (columnIndex) {
			0 -> b.image
			1 -> b.english_name.ifEmpty { b.ui_name }
			2 -> b.english_desc.ifEmpty { b.ui_description }
			else -> "[unknown]"
		}
	}

	override fun getColumnName(column: Int): String {
		return when (column) {
			0 -> "img"
			1 -> "name"
			2 -> "desc"
			else -> "[unknown]"
		}
	}

	override fun getColumnClass(columnIndex: Int): Class<*> {
		return when (columnIndex) {
			0 -> BufferedImage::class.java
			else -> String::class.java
		}
	}

	fun setPerks(lst: List<Perk>) {
		items = lst
		this.fireTableDataChanged()
	}

	fun perkAt(row: Int) : Perk? {
		if (row < 0 || row >= items.size) return null
		return items[row]
	}
}