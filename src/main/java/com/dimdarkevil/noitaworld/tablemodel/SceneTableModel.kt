package com.dimdarkevil.noitaworld.tablemodel

import com.dimdarkevil.noitaworld.model.Scene
import javax.swing.table.AbstractTableModel

class SceneTableModel : AbstractTableModel() {
	private var items = listOf<Scene>()

	override fun getRowCount() : Int {
		return items.size
	}

	override fun getColumnCount() : Int {
		return 5
	}

	override fun getValueAt(rowIndex: Int, columnIndex: Int): Any? {
		val b = items[rowIndex]
		return when (columnIndex) {
			0 -> b.name
			1 -> b.x
			2 -> b.y
			3 -> b.w
			4 -> b.h
			else -> "[unknown]"
		}
	}

	override fun getColumnName(column: Int): String {
		return when (column) {
			0 -> "name"
			1 -> "x"
			2 -> "y"
			3 -> "w"
			4 -> "h"
			else -> "[unknown]"
		}
	}

	fun setScenes(lst: List<Scene>) {
		items = lst
		this.fireTableDataChanged()
	}

	fun getSceneAt(row: Int) : Scene? {
		if (row < 0 || row >= items.size) return null
		return items[row]
	}

}