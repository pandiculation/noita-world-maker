package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.AppConfig
import com.dimdarkevil.swingutil.OkCancelModalDialog
import java.awt.BorderLayout
import java.io.File
import java.lang.RuntimeException
import javax.swing.*
import javax.swing.border.EmptyBorder
import javax.swing.filechooser.FileNameExtensionFilter

class SettingsDlg(val config: AppConfig, owner: JFrame, title: String) : OkCancelModalDialog<Pair<String,String>>(owner, title) {
	private val saveFolderLabel = JLabel(config.noitaSaveFolder)
	private val saveFolderBtn = JButton("select")
	private val exeLocationLabel = JLabel(config.noitaExeFile)
	private val exeLocationBtn = JButton("select")

	override fun buildUi(): JPanel {
		val pathsPanel = JPanel()
		pathsPanel.layout = BoxLayout(pathsPanel, BoxLayout.Y_AXIS)

		val savePanel = JPanel()
		savePanel.layout = BorderLayout(2, 2)
		savePanel.border = EmptyBorder(0, 5, 0, 5)
		savePanel.add(JLabel("game save path"), BorderLayout.WEST)
		savePanel.add(saveFolderLabel, BorderLayout.CENTER)
		savePanel.add(saveFolderBtn, BorderLayout.EAST)

		val exePanel = JPanel()
		exePanel.layout = BorderLayout(2, 2)
		exePanel.border = EmptyBorder(0, 5, 0, 5)
		exePanel.add(JLabel("game exe location"), BorderLayout.WEST)
		exePanel.add(exeLocationLabel, BorderLayout.CENTER)
		exePanel.add(exeLocationBtn, BorderLayout.EAST)

		pathsPanel.add(savePanel)
		pathsPanel.add(exePanel)

		saveFolderBtn.addActionListener { onSaveFolderBtnClick() }
		exeLocationBtn.addActionListener { onExeLocationBtnClick() }

		return pathsPanel
	}

	override fun getResult(): Pair<String,String> {
		val saveFile = File(saveFolderLabel.text)
		val exeFile = File(exeLocationLabel.text)
		if (!saveFile.exists()) {
			throw RuntimeException("save path ${saveFolderLabel.text} does not exist")
		}
		if (!saveFile.isDirectory()) {
			throw RuntimeException("save path ${saveFolderLabel.text} is not a directory")
		}
		if (!exeFile.exists()) {
			throw RuntimeException("game exe location ${exeLocationLabel.text} does not exist")
		}
		return Pair(saveFolderLabel.text, exeLocationLabel.text)
	}


	private fun onSaveFolderBtnClick() {
		val selDlg = JFileChooser()
		selDlg.currentDirectory = if (config.noitaExeFile.isNotBlank()) File(config.noitaSaveFolder) else HOME
		selDlg.isFileHidingEnabled = false
		selDlg.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
		selDlg.selectedFile = File(saveFolderLabel.text)
		if (selDlg.showOpenDialog(owner) == JFileChooser.APPROVE_OPTION) {
			val f = selDlg.selectedFile ?: return
			saveFolderLabel.text = f.path
		}
	}

	private fun onExeLocationBtnClick() {
		val selDlg = JFileChooser()
		selDlg.currentDirectory = if (config.noitaExeFile.isNotBlank()) File(config.noitaExeFile).parentFile else HOME
		selDlg.isFileHidingEnabled = false
		selDlg.fileSelectionMode = JFileChooser.FILES_ONLY
		selDlg.fileFilter = FileNameExtensionFilter("Exe file (.exe)", "exe")
		if (selDlg.showOpenDialog(owner) == JFileChooser.APPROVE_OPTION) {
			val f = selDlg.selectedFile ?: return
			if (f.name != "noita.exe" && f.name != "noita_dev.exe") {
				showError("File must be noita.exe or noita_dev.exe")
			}
			if (!f.exists()) {
				showError("noita.exe does not exist at chosen location")
				return
			}
			exeLocationLabel.text = f.path
		}
	}

	private fun showError(msg: String) {
		JOptionPane.showMessageDialog(owner, msg, "Error", JOptionPane.ERROR_MESSAGE)
	}

	private fun showInfo(msg: String, title: String = "Info") {
		JOptionPane.showMessageDialog(owner, msg, title, JOptionPane.INFORMATION_MESSAGE)
	}

}