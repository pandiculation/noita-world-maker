package com.dimdarkevil.noitaworld.listener

import com.dimdarkevil.noitaworld.model.Scene

interface MapStatusListener {
	fun showStatus(wx: Int, wy: Int, bx: Int, by: Int, msg: String)
	fun scenePosChanged(scene: Scene)
	fun playerPosChangeed(px: Int, py: Int)
}