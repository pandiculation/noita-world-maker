package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.AppConfig
import com.dimdarkevil.noitaworld.model.Scene
import com.dimdarkevil.noitaworld.model.Project
import com.dimdarkevil.swingutil.*
import java.awt.Color
import java.awt.Dimension
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.filechooser.FileNameExtensionFilter

class NewSceneDlg(owner: JFrame, title: String, val config: AppConfig, val project: Project) : OkCancelModalDialog<Scene>(owner, title) {
	private val nameTextField = JTextField()
	private val newCheckBox = JCheckBox("create blank (check this to create blank images instead of selecting existing ones)")

	private val widthLabel = JLabel("width")
	private val widthSpinner = JSpinner()
	private val heightLabel = JLabel("width")
	private val heightSpinner = JSpinner()

	private val bkgLabel = JLabel("Background image:")
	private val bkgFileLabel = JLabel("")
	private val bkgFileBtn = JButton("select")
	private val visLabel = JLabel("Visual image:")
	private val visFileLabel = JLabel("")
	private val visFileBtn = JButton("select")
	private val matLabel = JLabel("Material image:")
	private val matFileLabel = JLabel("")
	private val matFileBtn = JButton("select")

	private var lastDir : File? = null

	override fun buildUi(): JPanel {
		bkgFileLabel.background = Color.YELLOW
		preferredSize = Dimension(EmUnit.EM_SIZE * 60, bkgLabel.preferredSize.height * 24)
		nameTextField.preferredSize = Dimension(EmUnit.EM_SIZE * 30, nameTextField.preferredSize.height)
		//bkgFileLabel.preferredSize = Dimension(EmUnit.EM_SIZE * 40, bkgFileLabel.preferredSize.height)
		//visFileLabel.preferredSize = Dimension(EmUnit.EM_SIZE * 40, visFileLabel.preferredSize.height)

		widthSpinner.model = SpinnerNumberModel(256, 100, 512, 1)
		heightSpinner.model = SpinnerNumberModel(256, 100, 512, 1)

		val scenePanel = JPanel()
		scenePanel.layout = BoxLayout(scenePanel, BoxLayout.Y_AXIS)

		scenePanel.add(flowPanelWithMult(JLabel("Scene name"), nameTextField, JLabel("(lower-case, no spaces)")))
		scenePanel.add(flowPanelWith(newCheckBox))
		scenePanel.add(flowPanelWithMult(widthLabel, widthSpinner))
		scenePanel.add(flowPanelWithMult(heightLabel, heightSpinner))

		scenePanel.add(flowPanelWith(JLabel("(if selecting existing images, you must select at least a visual image, and the dimensions")))
		scenePanel.add(flowPanelWith(JLabel("of all selected images must be the same)")))
		scenePanel.add(flowPanelWithMult(bkgLabel, bkgFileBtn))
		scenePanel.add(flowPanelWith(bkgFileLabel))
		scenePanel.add(flowPanelWithMult(visLabel, visFileBtn))
		scenePanel.add(flowPanelWith(visFileLabel))
		scenePanel.add(flowPanelWithMult(matLabel, matFileBtn))
		scenePanel.add(flowPanelWith(matFileLabel))


		newCheckBox.addActionListener { enDisableControls() }
		bkgFileBtn.addActionListener { selectBkgImage() }
		visFileBtn.addActionListener { selectVisImage() }
		matFileBtn.addActionListener { selectMatImage() }

		enDisableControls()
		return scenePanel
	}

	private fun selectBkgImage() {
		val f = selectImage() ?: return
		bkgFileLabel.text = f.canonicalPath
	}

	private fun selectVisImage() {
		val f = selectImage() ?: return
		visFileLabel.text = f.canonicalPath
	}

	private fun selectMatImage() {
		val f = selectImage() ?: return
		matFileLabel.text = f.canonicalPath
	}

	private fun selectImage() : File? {
		val fc = JFileChooser()
		fc.currentDirectory = lastDir ?: getProjectFolder(config, project)
		fc.isMultiSelectionEnabled = false
		fc.fileFilter = FileNameExtensionFilter("PNG files", "png")
		val res = fc.showOpenDialog(owner)
		if (res != JFileChooser.APPROVE_OPTION) return null
		val f = fc.selectedFile ?: return null
		lastDir = f.parentFile
		return f
	}

	private fun enDisableControls() {
		if (newCheckBox.isSelected) {
			widthLabel.isEnabled = true
			widthSpinner.isEnabled = true
			heightLabel.isEnabled = true
			heightSpinner.isEnabled = true

			bkgFileLabel.isEnabled = false
			bkgLabel.isEnabled = false
			bkgFileBtn.isEnabled = false
			visFileLabel.isEnabled = false
			visLabel.isEnabled = false
			visFileBtn.isEnabled = false
		} else {
			widthLabel.isEnabled = false
			widthSpinner.isEnabled = false
			heightLabel.isEnabled = false
			heightSpinner.isEnabled = false

			bkgFileLabel.isEnabled = true
			bkgLabel.isEnabled = true
			bkgFileBtn.isEnabled = true
			visFileLabel.isEnabled = true
			visLabel.isEnabled = true
			visFileBtn.isEnabled = true
		}
	}

	data class ImgInfo(
		val bkgFile: BufferedImage,
		val visFile: BufferedImage,
		val matFile: BufferedImage,
		val width: Int,
		val height: Int,
	)

	override fun getResult(): Scene {
		val name = nameTextField.text.trim()
		if (name.isEmpty()) throw RuntimeException("enter a name (lower-case, no spaces)")
		if (name != name.lowercase()) throw RuntimeException("name must be all lowercase")
		if (name.contains(" ")) throw RuntimeException("name cannot contain spaces")
		val imgInfo = if (newCheckBox.isSelected) {
			// create blank
			val imgWidth = widthSpinner.value as Int
			val imgHeight = heightSpinner.value as Int
			if (imgWidth > 1024 || imgHeight > 1024) throw RuntimeException("Width and height must be <= 1024 pixels")
			val bi = BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_ARGB)
			ImgInfo(bi, bi, bi, imgWidth, imgHeight)
		} else {
			val bkgFilename = bkgFileLabel.text.trim()
			val visFilename = visFileLabel.text.trim()
			val matFilename = matFileLabel.text.trim()

			if (visFilename.isEmpty()) throw RuntimeException("select a visual image")
			val visFile = ImageIO.read(File(visFilename))

			val bkgFile = if (bkgFilename.isEmpty()) {
				BufferedImage(visFile.width, visFile.height, BufferedImage.TYPE_INT_ARGB)
			} else {
				ImageIO.read(File(bkgFilename))
			}

			val matFile = if (matFilename.isEmpty()) {
				BufferedImage(visFile.width, visFile.height, BufferedImage.TYPE_INT_ARGB)
			} else {
				ImageIO.read(File(matFilename))
			}

			if (bkgFile.width != visFile.width || bkgFile.height != visFile.height || matFile.width != visFile.width || matFile.height != visFile.height) {
				throw RuntimeException("dimensions of images must be the same")
			}
			ImgInfo(bkgFile, visFile, matFile, visFile.width, visFile.height)
		}
		val projFolder = getProjectFolder(config, project)
		val imgFolder = File(projFolder, "files/pixel_scenes")
		imgFolder.mkdirs()
		ImageIO.write(imgInfo.bkgFile, "png", File(imgFolder, "${name}_background.png"))
		ImageIO.write(imgInfo.visFile, "png", File(imgFolder, "${name}_visual.png"))
		ImageIO.write(imgInfo.matFile, "png", File(imgFolder, "${name}_material.png"))
		return Scene(name, 0, 0, imgInfo.matFile.width, imgInfo.matFile.height)
	}

}