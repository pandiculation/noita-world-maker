package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.BiomeMap
import com.dimdarkevil.noitaworld.model.Project
import com.dimdarkevil.swingutil.EmUnit
import com.dimdarkevil.swingutil.OkCancelModalDialog
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.image.BufferedImage
import javax.swing.*
import javax.swing.border.EmptyBorder

class NewMapDlg(owner: JFrame, title: String) : OkCancelModalDialog<Project>(owner, title) {
	private val modNameTextField = JTextField()
	private val modDescTextField = JTextField()
	private val worldNameTextField = JTextField()
	private val worldDescTextField = JTextArea()
	private val widthSpinner = JSpinner()
	private val heightSpinner = JSpinner()
	private val startySpinner = JSpinner()

	override fun buildUi(): JPanel {
		preferredSize = Dimension(EmUnit.EM_SIZE * 50, modNameTextField.preferredSize.height * 15)
		modNameTextField.preferredSize = Dimension(EmUnit.EM_SIZE * 20, modNameTextField.preferredSize.height)
		modDescTextField.preferredSize = Dimension(EmUnit.EM_SIZE * 30, modDescTextField.preferredSize.height)
		worldNameTextField.preferredSize = Dimension(EmUnit.EM_SIZE * 20, modNameTextField.preferredSize.height)
		worldDescTextField.preferredSize = Dimension(EmUnit.EM_SIZE * 30, modDescTextField.preferredSize.height * 4)
		worldDescTextField.lineWrap = true
		worldDescTextField.font = worldNameTextField.font

		val itemsPanel = JPanel()
		itemsPanel.layout = BoxLayout(itemsPanel, BoxLayout.Y_AXIS)

		widthSpinner.model = SpinnerNumberModel(70, 1, 1000, 1)
		heightSpinner.model = SpinnerNumberModel(48, 1, 1000, 1)
		startySpinner.model = SpinnerNumberModel(14, 0, 999, 1)

		val namePanel = JPanel()
		//namePanel.layout = BorderLayout(2, 2)
		namePanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		namePanel.border = EmptyBorder(0, 5, 0, 5)
		namePanel.add(JLabel("mod name:"))
		namePanel.add(modNameTextField)
		namePanel.add(JLabel("(lower-case, no spaces)"))

		val descPanel = JPanel()
		//descPanel.layout = BorderLayout(2, 2)
		descPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		descPanel.border = EmptyBorder(0, 5, 0, 5)
		descPanel.add(JLabel("mod description:"))
		descPanel.add(modDescTextField)
		//descPanel.add(JLabel("(lower-case, no spaces)"), BorderLayout.EAST)

		val wnamePanel = JPanel()
		//namePanel.layout = BorderLayout(2, 2)
		wnamePanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		wnamePanel.border = EmptyBorder(0, 5, 0, 5)
		wnamePanel.add(JLabel("world name:"))
		wnamePanel.add(worldNameTextField)
		wnamePanel.add(JLabel("(shows on the menu)"))

		val wdescPanel = JPanel()
		//descPanel.layout = BorderLayout(2, 2)
		wdescPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		wdescPanel.border = EmptyBorder(0, 5, 0, 5)
		wdescPanel.add(JLabel("world description:"))
		wdescPanel.add(worldDescTextField)

		val widthPanel = JPanel()
		//widthPanel.layout = BorderLayout(2, 2)
		widthPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		widthPanel.border = EmptyBorder(0, 5, 0, 5)
		widthPanel.add(JLabel("map width in blocks:"))
		widthPanel.add(widthSpinner)
		widthPanel.add(JLabel("(max 100)"))

		val heightPanel = JPanel()
		//heightPanel.layout = BorderLayout(2, 2)
		heightPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		heightPanel.border = EmptyBorder(0, 5, 0, 5)
		heightPanel.add(JLabel("map height in blocks:"))
		heightPanel.add(heightSpinner)
		heightPanel.add(JLabel("(max 100)"))

		val startyPanel = JPanel()
		//startyPanel.layout = BorderLayout(2, 2)
		startyPanel.layout = FlowLayout(FlowLayout.LEADING, 2, 2)
		startyPanel.border = EmptyBorder(0, 5, 0, 5)
		startyPanel.add(JLabel("start Y pos in blocks:"))
		startyPanel.add(startySpinner)

		itemsPanel.add(namePanel)
		itemsPanel.add(descPanel)
		itemsPanel.add(wnamePanel)
		itemsPanel.add(wdescPanel)
		itemsPanel.add(widthPanel)
		itemsPanel.add(heightPanel)
		itemsPanel.add(startyPanel)

		return itemsPanel
	}

	override fun getResult(): Project {
		if (modNameTextField.text.trim().contains(" ")) {
			throw RuntimeException("Mod name cannot contain spaces")
		}
		if (modNameTextField.text != modNameTextField.text.lowercase()) {
			throw RuntimeException("Mod name must be lower-case")
		}
		val w = widthSpinner.value as Int
		val h = heightSpinner.value as Int
		val sy = startySpinner.value as Int
		if (sy >= h) throw RuntimeException("start y must be less than height")
		val bi = BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
		(0 until h).forEach { y ->
			(0 until w).forEach { x ->
				bi.setRGB(x, y, DEFAULT_BIOME_COLOR)
			}
		}
		return Project(
			name = modNameTextField.text.trim(),
			description = modDescTextField.text.trim(),
			pretty_name = worldNameTextField.text.trim().ifEmpty { modNameTextField.text.trim() },
			pretty_description = worldDescTextField.text.trim().ifEmpty { modDescTextField.text.trim() },
			biome_map = BiomeMap(width = w, height = h, startY = sy, bi = bi)
		)
	}

}