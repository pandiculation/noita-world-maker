package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.listener.MapStatusListener
import com.dimdarkevil.noitaworld.model.*
import java.awt.*
import java.awt.event.*
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import javax.swing.JPanel
import kotlin.math.sqrt


class MapDrawPanel(private val statusListener: MapStatusListener) : JPanel(), ComponentListener, MouseListener, MouseMotionListener, MouseWheelListener {
	private var bufferedImage : BufferedImage? = null
	private var pixelWidth = 0
	private var pixelHeight = 0
	private val viewport = Viewport(pixelWidth * 512, pixelHeight * 512, 0.015625)
	private var lastMouseX = -1
	private var lastMouseY = -1
	private var dragging = false
	private var panning = false
	//private val popupMenu = JPopupMenu()
	//val newVarMenuItem = JMenuItem("new variable")
	private val popupPos = Vec2i(0, 0)
	private var biWidth = 0 // prevents having to reference the image all the time
	private var biHeight = 0 // prevents having to reference the image all the time
	private val atf = AffineTransform()
	private val emptyBiome = Biome("[not found]", 0, "", 0)
	private val lastMovePos = Vec2i(-1, -1)
	var biomeMap = mapOf<Int, Biome>()
	var scenes = listOf<Scene>()
	var paintColor = DEFAULT_BIOME_COLOR
	private var drawMode = DrawMode.DRAW
	private val texBi = javaClass.classLoader.getResourceAsStream("pattern.png")?.use { sin ->
		ImageIO.read(sin)
	} ?: throw RuntimeException("could not load pattern.png")
	private val pattern = TexturePaint(texBi, Rectangle(0, 0, texBi.width, texBi.height))
	val halfTransparent = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f)

	private var startY = 0
	private var playerStartPos = Vec2i(0, 0)
	private var playerViewportPos = Vec2i(0, 0)
	private var draggingPlayer = false
	private var draggingScene: Scene? = null
	private var dragSceneOffX = 0
	private var dragSceneOffY = 0
	private val holdPos = Vec2i(0, 0)
	private val playerCircleColor = Color(0x80FFFF00.toInt())

	private val undos = mutableListOf<Undo>()

	init {
		addComponentListener(this)
		addMouseWheelListener(this)
		addMouseListener(this)
		addMouseMotionListener(this)
		//popupMenu.add(newVarMenuItem)
		//newVarMenuItem.addActionListener { addVariable() }
		isOpaque = true
	}

	override fun paintComponent(gg: Graphics) {
		super.paintComponent(gg)
		val bi = bufferedImage ?: return
		val g = gg as Graphics2D
		g.font = font

		val scale = viewport.getScale()
		val tl = viewport.getTopLeft()
		val br = viewport.getBottomRight()
		var sx = maxOf(0, (tl.x / 512.0).toInt())-1
		var sy = maxOf(0, (tl.y / 512.0).toInt())-1
		var ex = minOf((br.x / 512.0).toInt(), bi.width)+1
		var ey = minOf((br.y / 512.0).toInt(), bi.height)+1
		if (sx < 0) sx = 0
		if (sy < 0) sy = 0
		if (ex > bi.width) ex = bi.width
		if (ey > bi.height) ey = bi.height
		(sy until ey).forEach { y ->
			(sx until ex).forEach { x ->
				val ci = bi.getRGB(x, y)
				val tf = g.transform
				// reset our affine transform (rather than creating garbage on each draw)
				atf.setToIdentity()
				atf.scale(scale, scale)
				atf.translate(-tl.x, -tl.y)
				g.transform(atf)
				//g.stroke = stroke
				g.color = Color(ci)
				g.fillRect(x*512, y*512, 512, 512)
				g.transform = tf
			}
		}
		(sy until ey).forEach { y ->
			(sx until ex).forEach { x ->
				val tf = g.transform
				atf.setToIdentity()
				atf.scale(scale, scale)
				atf.translate(-tl.x, -tl.y)
				g.transform(atf)
				g.color = Color.BLACK
				g.stroke = BasicStroke( (1.0 / scale).toFloat(), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER )
				g.drawRect(x*512, y*512, 512, 512)
				g.transform = tf
			}
		}
		scenes.forEach { scene ->
			val scx = scene.x + (512 * (biWidth / 2))
			val scy = scene.y + (512 * startY)
			if (viewport.inBounds(scx.toDouble(), scy.toDouble(), scene.w.toDouble(), scene.h.toDouble())) {
				val tf = g.transform
				atf.setToIdentity()
				atf.scale(scale, scale)
				atf.translate(-tl.x, -tl.y)
				g.transform(atf)
				g.color = Color.ORANGE
				g.paint = pattern
				g.fillRect(scx, scy, scene.w, scene.h)
				g.color = Color.BLACK
				g.drawRect(scx, scy, scene.w, scene.h)
				g.transform = tf
			}
		}

		val tf = g.transform
		atf.setToIdentity()
		atf.scale(scale, scale)
		atf.translate(-tl.x, -tl.y)
		g.transform(atf)
		g.color = playerCircleColor
		val holdComp = g.composite
		g.composite = halfTransparent
		g.fillOval(playerViewportPos.x-50, playerViewportPos.y-50, 100, 100)
		g.color = Color.BLACK
		g.stroke = BasicStroke( (1.0 / scale).toFloat(), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER )
		g.drawOval(playerViewportPos.x-50, playerViewportPos.y-50, 100, 100)
		g.fillOval(playerViewportPos.x-8, playerViewportPos.y-8, 16, 16)
		g.composite = holdComp
		g.transform = tf
	}

	// ComponentListener methods
	override fun componentResized(e: ComponentEvent) {
		pixelWidth = e.component.width
		pixelHeight = e.component.height
		viewport.resize(pixelWidth, pixelHeight)
	}
	override fun componentHidden(e: ComponentEvent) { }
	override fun componentMoved(e: ComponentEvent) { }
	override fun componentShown(e: ComponentEvent) { }

	override fun mouseClicked(e: MouseEvent) {
		when (e.button) {
			MouseEvent.BUTTON1 -> {
			}
			MouseEvent.BUTTON2 -> { }
			MouseEvent.BUTTON3 -> {
/*
				popupPos.x = e.x
				popupPos.y = e.y
				popupMenu.show(e.component, e.x, e.y)
*/
			}
		}
	}

	override fun mouseWheelMoved(e: MouseWheelEvent) {
		val changeAmount = -((e.wheelRotation.toDouble() * 0.04))
		var newScale = viewport.getScale() * ( 1.0 + changeAmount)
		if (newScale > 100.0) newScale = 100.0
		if (newScale <= 0.0) newScale = 0.0
		//if (newScale <= 1.0) newScale = 1.0
		//println("mouseWheelMoved ${e.wheelRotation} ${newScale}")
		//viewport.rescale(newScale)
		viewport.rescaleAroundPoint(e.x, e.y, newScale)
		repaint()
	}

	override fun mousePressed(e: MouseEvent) {
		drawMode = if (e.isShiftDown) DrawMode.MOVE else DrawMode.DRAW
		if (e.button == MouseEvent.BUTTON2) {
			//println("mousePressed ${e.button} ${MouseEvent.BUTTON1}")
			panning = true
			lastMouseX = e.x
			lastMouseY = e.y
		} else if (e.button == MouseEvent.BUTTON1) {
			dragging = true
			when (drawMode) {
				DrawMode.DRAW -> {
					undos.add(Undo())
					drawPixelFromMouseEvent(e)
				}
				DrawMode.MOVE -> {
					viewport.getWorldPosInt(e.x, e.y).let { wp ->
						if (isMouseOverPlayer(wp.x, wp.y)) {
							draggingPlayer = true
							holdPos.x = playerStartPos.x
							holdPos.y = playerStartPos.y
						} else {
							for(sc in scenes) {
								if (isMouseOverScene(wp.x, wp.y, sc)) {
									draggingScene = sc
									holdPos.x = sc.x
									holdPos.y = sc.y
									dragSceneOffX = (sc.x + (512 * (biWidth / 2))) - wp.x
									dragSceneOffY =  (sc.y + (512 * startY)) - wp.y
									break
								}
							}
						}
					}
				}
			}
		}
	}

	override fun mouseDragged(e: MouseEvent) {
		if (!panning && !dragging) return
		val bi = bufferedImage ?: return
		//println("mouseMoved")
		if (panning) {
			viewport.moveByPix(lastMouseX-e.x, lastMouseY-e.y)
			repaint()
			lastMouseX = e.x
			lastMouseY = e.y
		} else if (dragging) {
			when (drawMode) {
				DrawMode.DRAW -> drawPixelFromMouseEvent(e)
				DrawMode.MOVE -> {
					if (draggingPlayer) {
						viewport.getWorldPosInt(e.x, e.y).let { wp ->
							playerViewportPos.x = wp.x
							playerViewportPos.y = wp.y
							updatePlayerStartPos()
							repaint(e.x-100, e.y-100, 200, 200)
						}
					} else if (draggingScene != null) {
						val sc = draggingScene ?: return
						viewport.getWorldPosInt(e.x, e.y).let { wp ->
							sc.x = (wp.x - (512 * (bi.width / 2))) + dragSceneOffX
							sc.y = (wp.y - (512 * startY)) + dragSceneOffY
							repaint((e.x+dragSceneOffX)-50, (e.y+dragSceneOffY)-50, sc.w+100, sc.h+100)
						}
					}
				}
			}
		}
	}

	override fun mouseMoved(e: MouseEvent) {
		val bi = bufferedImage ?: return
		viewport.getWorldPosInt(e.x, e.y).let { wp ->
			val wo = (biWidth / 2)
			val ho = (startY)

			val x = wp.x
			val y = wp.y

			val xx = (x / 512)
			val yy = (y / 512)

			val wx = wp.x - (wo * 512)
			val wy = wp.y - (ho * 512)

			if (lastMovePos.x == wx && lastMovePos.y == wy) return
			lastMovePos.x = wx
			lastMovePos.y = wy
			if (xx >= 0 && xx < biWidth && yy >= 0 && yy < biHeight) {
				val ci = bi.getRGB(xx, yy)
				val biome = biomeMap[ci] ?: emptyBiome
				val msg = scenes.firstOrNull {
					(wx >= it.x && wx < (it.x+it.w) && wy >= it.y && wy < (it.y+it.h))
				}?.let { "scene: ${it.name}" } ?: "${biome.biome_filename} (${biome.color})"
				statusListener.showStatus(wx, wy, xx-wo, yy-ho, msg)
			}
		}
	}

	override fun mouseReleased(e: MouseEvent) {
		if (draggingPlayer) {
			statusListener.playerPosChangeed(playerStartPos.x, playerStartPos.y)
			undos.add(Undo(mutableListOf(PlayerStartChange(holdPos.x, holdPos.y, playerStartPos.x, playerStartPos.y))))
		} else if (draggingScene != null) {
			draggingScene?.let {
				statusListener.scenePosChanged(it)
				undos.add(Undo(mutableListOf(SceneChange(it, holdPos.x, holdPos.y, it.x, it.y))))
			}
		}
		if (undos.isNotEmpty() && undos[undos.size-1].changes.isEmpty()) {
			undos.removeLast()
		}
		panning = false
		dragging = false
		draggingPlayer = false
		draggingScene = null
	}

	override fun mouseEntered(e: MouseEvent) {
	}

	override fun mouseExited(e: MouseEvent) {
	}

	private fun drawPixelFromMouseEvent(e: MouseEvent) {
		bufferedImage?.let { bi ->
			viewport.getWorldPos(e.x, e.y).let { wp ->
				val x = (wp.x / 512.0).toInt()
				val y = (wp.y / 512.0).toInt()
				if (x >= 0 && x < bi.width && y >= 0 && y < bi.height) {
					val curRgb = bi.getRGB(x, y)
					bi.setRGB(x, y, paintColor)
					if (curRgb != paintColor) undos[undos.size-1].changes.add(PixelChange(x, y, curRgb, paintColor))
					val pp = viewport.getPixelPos((x*512).toDouble(), (y*512).toDouble())
					val scale = ((viewport.getScale() * 512.0) + 20.0).toInt()
					repaint(pp.x, pp.y, scale, scale)
				}
			}
		}
	}

	fun setMap(bi: BufferedImage, sy: Int, psx: Int, psy: Int) {
		bufferedImage = bi
		biWidth = bi.width
		biHeight = bi.height
		startY = sy
		playerStartPos.x = psx
		playerStartPos.y = psy
		updatePlayerViewPos()
		viewport.resize(this.width, this.height)
		repaint()
	}

	fun fitImage() {
		bufferedImage?.let { bi ->
			viewport.fitImage(bi.width * 512, bi.height * 512)
		}
	}

	fun setPixelScenes(slist: List<Scene>) {
		scenes = slist
		repaint()
	}

	fun getImage() = bufferedImage

	fun undo() {
		val bi = bufferedImage ?: return
		if (undos.isEmpty()) return
		val undo = undos.removeLast()
		undo.changes.forEach { ch ->
			when (ch) {
				is PixelChange -> bi.setRGB(ch.x, ch.y, ch.fromColor)
				is PlayerStartChange -> {
					playerStartPos.x = ch.fromX
					playerStartPos.y = ch.fromY
					updatePlayerViewPos()
				}
				is SceneChange -> {
					ch.scene.x = ch.fromX
					ch.scene.y = ch.fromY
					statusListener.scenePosChanged(ch.scene)
				}
				is ObjectChange -> { }
			}

		}
		repaint()
	}

	private fun updatePlayerViewPos() {
		playerViewportPos.x = playerStartPos.x + (512 * (biWidth / 2))
		playerViewportPos.y = playerStartPos.y + (512 * startY)
	}

	private fun updatePlayerStartPos() {
		playerStartPos.x = playerViewportPos.x - (512 * (biWidth / 2))
		playerStartPos.y = playerViewportPos.y - (512 * startY)
	}

	private fun isMouseOverPlayer(mx: Int, my: Int) : Boolean {
		val xd = (mx - playerViewportPos.x).toDouble()
		val yd = (my - playerViewportPos.y).toDouble()
		val dist = sqrt((xd * xd) + (yd * yd))
		//println("dist is $dist")
		return (dist < 50.0)
	}

	private fun isMouseOverScene(mx: Int, my: Int, sc: Scene) : Boolean {
		val vx = sc.x + (512 * (biWidth / 2))
		val vy = sc.y + (512 * startY)
		return (mx >= vx && mx < (vx+sc.w) && my >= vy && my < (vy + sc.h))
	}

}