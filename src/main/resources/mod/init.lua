dofile("data/scripts/lib/utilities.lua")
dofile( "data/scripts/perks/perk.lua" )

function OnPlayerSpawned( player_entity )
	if (GameHasFlagRun("%modname%")) then return end
	GameAddFlagRun("%modname%")

	--ConvertMaterialEverywhere( CellFactory_GetType("magic_liquid_teleportation"), CellFactory_GetType("water") )

	initPlayer(player_entity)
	loadPixelScenes()
	loadEntities()

end

function OnModPreInit()
    -- SetWorldSeed(1553189131)
end

function OnWorldPreUpdate()
	local x, y = EntityGetTransform(GetPlayer())
	-- GamePrint("Player Position X/Y: " .. tostring(x) .. " / " .. tostring(y))
end

function OnWorldPostUpdate()
    --IsPlayerDead()
end


function GetPlayer()
	return EntityGetWithTag("player_unit")[1]
end

function initPlayer(player_entity)
	local playerX, playerY = %playerx%, %playery%
	GlobalsSetValue("respawn_x", playerX)
	GlobalsSetValue("respawn_y", playerY)
	EntitySetTransform(player_entity, playerX, playerY)
end

function loadEntities()
--load_entities_start
	-- portals
	-- EntityLoad("mods/obstaclecoursealpha/files/entities/buildings/portal_to_3.xml", -8414, 16980)
	-- EntityLoad("mods/obstaclecoursealpha/files/entities/buildings/portal_to_snowy.xml", -6250, 16700)
--load_entities_end
end


function loadPixelScenes()
--load_pixel_scenes_start
	--LoadPixelScene("mods/obstaclecoursealpha/files/pixel_scenes/blockade_HM_north.png", "", 160, 3040, "", true)
	--LoadPixelScene("mods/obstaclecoursealpha/files/pixel_scenes/blockade_vertical.png", "", -2580, 3580, "", true)
	--LoadPixelScene("mods/obstaclecoursealpha/files/pixel_scenes/blockade_vertical.png", "", 500, 3050, "", true)
	--LoadPixelScene("mods/obstaclecoursealpha/files/pixel_scenes/blockade_vertical.png", "", 500, 3562, "", true)
	--LoadPixelScene("mods/obstaclecoursealpha/files/pixel_scenes/blockade_vertical.png", "", 500, 4074, "", true)
--load_pixel_scenes_end
end

ModMagicNumbersFileAdd( "mods/%modname%/files/magic_numbers.xml" ) 