function get_player()
  return EntityGetWithTag("player_unit")[1]
end

function get_player_pos()
  local player = get_player()
  if not player then return 0, 0 end
  return EntityGetTransform(player)
end

function teleport(x, y)
  EntitySetTransform(get_player(), x, y)
end

function get_health()
  local dm = EntityGetComponent(get_player(), "DamageModelComponent")[1]
  return ComponentGetValue(dm, "hp"), ComponentGetValue(dm, "max_hp")
end

function set_health(cur_hp, max_hp)
  local damagemodels = EntityGetComponent(get_player(), "DamageModelComponent")
  for _, damagemodel in ipairs(damagemodels or {}) do
    ComponentSetValue(damagemodel, "max_hp", max_hp)
    ComponentSetValue(damagemodel, "hp", cur_hp)
  end
end

function quick_heal()
  local _, max_hp = get_health()
  set_health(max_hp, max_hp)
end

function get_money()
  local wallet = EntityGetFirstComponent(get_player(), "WalletComponent")
  return ComponentGetValue2(wallet, "money")
end

function set_money(amt)
  local wallet = EntityGetFirstComponent(get_player(), "WalletComponent")
  ComponentSetValue2(wallet, "money", amt)
end

function get_vector_value(comp, member, kind)
  kind = kind or "float"
  local n = ComponentGetVectorSize( comp, member, kind )
  if not n then return nil end
  local ret = {};
  for i = 1, n do
    ret[i] = ComponentGetVectorValue(comp, member, kind, i-1) or "nil"
  end
  return ret
end

function print_vector_value(...)
  local v = get_vector_value(...)
  if not v then return nil end
  return "{" .. table.concat(v, ", ") .. "}"
end

function print_component_info(c)
  local frags = {"<" .. ComponentGetTypeName(c) .. ">"}
  local members = ComponentGetMembers(c)
  if not members then return end
  for k, v in pairs(members) do
    table.insert(frags, k .. ': ' .. tostring(v))
  end
  print(table.concat(frags, '\n'))
end

function print_detailed_component_info(c)
  local members = ComponentGetMembers(c)
  if not members then return end
  local frags = {}
  for k, v in pairs(members) do
    if (not v) or #v == 0 then
      local mems = ComponentObjectGetMembers(c, k)
      if mems then
        table.insert(frags, k .. ">")
        for k2, v2 in pairs(mems) do
          table.insert(frags, "  " .. k2 .. ": " .. tostring(v2))
        end
      else
        v = print_vector_value(c, k)
      end
    end
    table.insert(frags, k .. ': ' .. tostring(v))
  end
  print(table.concat(frags, '\n'))

end

function print_entity_info(e)
  local comps = EntityGetAllComponents(e)
  if not comps then
    print("Invalid entity?")
    return
  end
  for idx, comp in ipairs(comps) do
    print(comp, "-----------------")
    print_component_info(comp)
  end
end
