dofile_once("data/scripts/lib/utilities.lua")
dofile_once( "data/scripts/gun/gun_actions.lua" )
dofile_once( "data/scripts/game_helpers.lua" )
dofile_once( "mods/%modname%/files/scripts/chest_spawns.lua" )
-------------------------------------------------------------------------------


function on_open( entity_item )
	local x, y = EntityGetTransform( entity_item )

	local entity_name = EntityGetName( entity_item )
	print("entity name is ", entity_name )

	local spawn_func = chest_spawn_funcs[entity_name]
	if (spawn_func) then
		spawn_func(x, y)
	else
		print("no function was found")
	end
	
	EntityLoad("data/entities/particles/image_emitters/chest_effect.xml", x, y)
end

function item_pickup( entity_item, entity_who_picked, name )
	GamePrintImportant( "$log_chest", "" )

	on_open( entity_item )
	
	EntityKill( entity_item )
end

function physics_body_modified( is_destroyed )
	local entity_item = GetUpdatedEntityID()
	
	on_open( entity_item )

	edit_component( entity_item, "ItemComponent", function(comp,vars)
		EntitySetComponentIsEnabled( entity_item, comp, false )
	end)
	
	EntityKill( entity_item )
end