dofile_once("mods/%modname%/files/scripts/util.lua")

function collision_trigger()
	local entity_id = GetUpdatedEntityID()
	local entity_name = EntityGetName( entity_id )
	print("entity name is ", entity_name )
	-- the entity name will be a name, an x pos and a y pos separated by underscore
	local name_parts = string.gmatch(entity_name, "([^_]+)")

	local just_name = name_parts()
	local remote_x = tonumber(name_parts())
	local remote_y = tonumber(name_parts())
	if (remote_x ~= nil and remote_y ~= nil) then
		print("-=-= teleporting player to", remote_x, remote_y)
		teleport(remote_x, remote_y)
	else
		print("-=-= no valid remote teleport location")
	end
end

