# noita-world-maker

World-maker utility for Noita

## Build
`noita-world-maker` uses Maven. From the root project folder, just do `mvn package`. This will create a `noita-world-maker-bin.zip` in the `target` folder.

## Run
Before running, you need to set Noita up for modding. In the folder where Noita is installed, there's a sub-folder called `tools_modding` that contains a `READ_ME_FIRST.txt` file with instructions. `noita-world-maker` uses the data extracted in this process to load all its information.

You'll also need to have a version 11 or higher Java JRE or JDK installed on your system. [I'm currently using this one](https://docs.microsoft.com/en-us/java/openjdk/download)

Extract `noita-world-maker-bin.zip` somewhere (like `\Program Files`), and run `noita-world-maker.bat` from the extracted `noita-world-maker` folder.

The first time you run the program, it will ask you to locate your Noita save folder (usually in `%homepath%\AppData\LocalLow\Nolla_Games_Noita`) and your Noita exe file (for Steam, usually `\Program Files (x86)\Steam\steamapps\common\Noita\noita.exe`). Once you set these, the program will exit. Run `noita-world-maker.bat` again to get started.

## How to Use
`noita-world-maker` is an attempt to make some of the tasks involved in creating a custom Noita map easier. It does not try to be a drawing program, but instead provides some simple ways of bringing in environment art that you have created in other programs.

A Noita world consists of two basic parts: a biome map and a set of pixel scenes.

### Biome Map
A biome map is an image with a single pixel for each 512x512-pixel square (called a "chunk") in the game world. The color of each pixel in the biome map references a biome *type* that indicates what sort of content the game should generate within that chunk. The set of biome types and their colors can be found in `{Noita Save Folder}\data\biome\_biomes_all.xml`, but `noita-world-maker` will handle that for you.

You can look at the default biome map for New Game mode in Noita in `{Noita Save Folder}\data\biome_impl\biome_map.png`. That image is 70 pixels wide and 48 pixels across. This means that the regular Noita world is 35,840 (512 * 70) pixels across. Noita worlds are "infinite" vertically, but the biome map defines 24,576 (512 * 48) pixels of world space.

The coordinate system of a Noita world is a bit strange. The x (horizontal) axis increases positively from left to right, and the y (vertical) axis increases positively from top to bottom. The origin point of a chunk is its top left corner. The world origin (the `(0, 0)` point) is halfway across the map in the x direction (for this reason, always use an even number of horizontal pixels for your biome maps) and a certain number of chunks down from the top of the biome map. That "certain number of chunks" is defined in `{Noita Save Folder}\data\biome\_biomes_all.xml` in an attribute named `biome_offset_y` of the root element. If you take a look, you can see that the normal value of `biome_offset_y` for Noita is 14.

When you create a new project, `noita_world_maker` will ask you to specify the number of chunks your world will be both horizontally and vertically, and what value should be used for `biome_offset_y`.

### Pixel Scenes
A pixel scene is a set of images drawn at actual game-world size, and overlayed onto the game. Pixel scene images are PNG files in 8-bit ARGB format (which means that transparency can be used). A pixel scene has three layers: a background layer; a material layer; and a visual layer.

The background layer doesn't interact with the game world and is just drawn behind your scene.

The visual layer is also drawn as-is on top of your scene.

The material layer is the interesting part. Like the biome map, the material layer uses colors as a reference, this time to what *material* that pixel is made of. There are over 400 distinct materials in Noita, which can be found (along with their respective colors and other properties) in `{Noita Save Folder}\data\materials.xml`. Materials have a default appearance, and don't need to be painted over in the visual layer to be visible. Liquid materials (like blood and sand) shouldn't be painted over because they move around.

As noted above, `noita-world-maker` doesn't try to be a drawing program (though you can draw individual pixels). There are two basic ways to do pixel scenes:

1. Draw your background, visual, and material layers in a drawing program like Krita or Aesprite, and import them into `noita-world-maker` (all three images must have the same dimensions).
2. Let `noita-world-maker` create empty (transparent) layers for you according to specified dimensions, and then edit them in a drawing program.

An idiosyncrasy of Noita's material color system is that no grey shades (colors with the same red, green and blue value) are used to reference a material. You can make use of this in `noita-world-maker` by painting material layers in your drawing program using distinct grey colors for each material, then replacing each grey shade with its correct material color in `noita-world-maker`. All you need to do is:

* right-click the grey color to be replaced in your pixel scene
* select the material you want that to be in the material list
* click the "replace" button

### Creating, opening and saving projects
When you create a new project in `noita-world-maker`, a mod is generated in the `{Noita exe folder}\mods` folder. An extra `noita-world-maker.json` is also created, and this is what you'll choose when you want to open the project. To save the project, just hit `save`, and any new code will be generated for you.

You can manually edit the `init.lua` file if you want, but leave the `--load_entities_start`, `--load_entities_end`, `--load_pixel_scenes_start` and `--load_pixel_scenes_end` comments (and their enclosing functions) intact. The contents between these comments will be regenerated on save, but you can add your own code above or below.

The `mods/{your mod}/files/scripts/chest_spawns.lua` file is regenerated wholesale on each save.

The mods created by `noita-world-maker` run as Noita "game modes", meaning that you click a button from the new game menu to play it, like you do with nightmare mode or the daily run. You can change this in `mods\{your mod}\mod.xml` by replacing `is_game_mode="1"` with `is_game_mode="0"`. The reason for using game modes is that it more clearly (to me) expresses the intent of the mod, which includes probable incompatability with other mods.

A placeholder game mode button is automatically generated to `mods/{your mod}/files/menu_banner_overlay.png` and `mods/{your mod}/files/menu_banner_background.png`. These are only generated once, so you can edit them as you like.

### Controls
In general, left-click draws pixels, middle-click pans the image, and scroll wheel zooms. Use shift-left-click to move objects around on the biome map or pixel scenes. On pixel scenes, right-click is used to select a material replacement color. Use Ctrl-Z to undo.

## Support
Report any issues with `noita-world-maker` on the [gitlab issues page](https://gitlab.com/alter_ukko/noita-world-maker/-/issues).

## Roadmap
* portals
* hit-boxes to trigger events
* switches, levers, etc
* custom entities, materials, spells and perks

## Contributing
Contributions are welcome. If you're thinking adding features or fixes, please contact me to coordinate (I'm `alter_ukko#9483` on Discord, and I can also be reached at `alter.ukko@gmail.com`).

## Acknowledgments
There is tons of cool stuff about modding and world-building for Noita out there, and I shamelessly make use of it all. I'd especially like to acknowledge:

* The folks who maintain and add information to [the Noita wiki](https://noita.wiki.gg/wiki/Noita_Wiki)
* kaliuresis, who made the excellent [Krita Noita Editor](https://github.com/kaliuresis/krita-noita-editor), along with many other very useful things.
* soler91, who made [Noita Together](https://github.com/soler91/noita-together), [Streamer Wands](https://github.com/soler91/streamer-wands) and a bunch of other stuff -- and whose code I often use to figure out how to do things.

